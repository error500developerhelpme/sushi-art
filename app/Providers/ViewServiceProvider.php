<?php

namespace App\Providers;

use App\Http\View\CountriesComposer;
use App\Http\View\GlobalComposer;
use App\Http\View\OrderComposer;
use App\Http\View\ShoppingCardComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        View::composer('*', CountriesComposer::class);
        View::composer('*', ShoppingCardComposer::class);
        View::composer('*', OrderComposer::class);
        View::composer('*', GlobalComposer::class);
    }
}
