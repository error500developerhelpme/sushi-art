<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryTranslate extends Model
{
    use HasFactory;

    protected $table = 'country_translates';

    protected $fillable = [
        'county_id',
        'lang',
        'name',
    ];
}
