<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends LocalizedModel
{
    use HasFactory;

    protected $table = 'countries';

    protected $fillable = [
        'lang',
        'path',
    ];
}
