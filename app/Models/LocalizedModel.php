<?php


namespace App\Models;


use App\Hellper\TranslateHellper;
use App\Http\Middleware\TranslateMiddleware;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalizedModel extends Model
{
    use HasFactory;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function localizations()
    {
        return $this->hasMany($this->getClassName());
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return get_class($this) . 'Translate';
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getAccesserAttribute()
    {
        $lang = session('lang');

        if (!$lang){
            $lang = env('APP_LANG');
        }
        $translate = $this->localizations()->where('lang', $lang)->first();
        if ($translate) {
            return $translate;
        }

        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translateLang()
    {
        // для того чтобы елси был PageItem стал page_item
        $str = lcfirst(class_basename($this));
        $str = preg_replace("/[A-Z]/", '_'.'$0', $str);
        $translateModel = get_class($this).'Translate';
        $lowercase = strtolower($str);
        return $this->hasMany($translateModel,$lowercase.'_id','id');
    }

}
