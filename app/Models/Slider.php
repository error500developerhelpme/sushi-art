<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends LocalizedModel
{
    use HasFactory;

    protected $table = 'sliders';

    protected $fillable = [
        'path',
        'status',
        'product_id',
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopeGetSlider(\Illuminate\Database\Eloquent\Builder $query)
    {
        return $query->where('status', 1);
    }
}
