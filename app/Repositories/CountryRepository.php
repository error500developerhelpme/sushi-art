<?php

namespace App\Repositories;

use App\Hellper\HellperNotifyMessage;
use App\Interfaces\HomeInterface;
use App\Models\Country;
use App\Models\Room;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class CountryRepository
{

    /**
     * @return mixed
     */
//    public function index(): mixed
//    {
//        return Room::query()->ActiveRooms();
//    }

    public function CreateOrUpdate($data, $id)
    {
//        try {
            DB::beginTransaction();
                $county = Country::query()->updateOrCreate(
                    ['id' => $id],
                    [
                        'lang' => $data['lang'],
                        'path' => $data['image'],
                    ]
                );
                foreach ($data['langs'] as $key => $lang){
                    $county->localizations()->updateOrCreate(
                        ['country_id' => $county->id, 'lang' => $key],
                        [
                            'country_id' => $county->id,
                            'lang' => $key,
                            'name' => $lang['name'],
                        ]
                    );
                }
            DB::commit();
            return [
                'message' => HellperNotifyMessage::messageNotify($id),
            ];
//        }catch (\Exception $error){
//            DB::rollBack();
//            return [
//                'message' => 'oops',
//            ];
//        }
    }

}
