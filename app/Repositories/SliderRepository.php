<?php

namespace App\Repositories;

use App\Hellper\HellperNotifyMessage;
use App\Models\Slider;
use Illuminate\Support\Facades\DB;

class SliderRepository
{
    /**
     * @param $data
     * @param $id
     * @return array
     */
    public function CreateOrUpdate($data, $id)
    {
        try {
            DB::beginTransaction();
                $slider = Slider::query()->updateOrCreate(
                    ['id' => $id],
                    [
                        'path' => $data['image'],
                        'product_id' => $data['product_id'],
                    ]
                );
                foreach ($data['langs'] as $key => $lang){
                    $slider->localizations()->updateOrCreate(
                        ['slider_id' => $slider->id, 'lang' => $key],
                        [
                            'slider_id' => $slider->id,
                            'lang' => $key,
                            'title' => $lang['title'],
                            'sub_title' => $lang['sub_title'],
                        ]
                    );
                }
            DB::commit();
            return [
                'message' => HellperNotifyMessage::messageNotify($id),
            ];
        }catch (\Exception $error){
            DB::rollBack();
            return [
                'message' => 'oops',
            ];
        }
    }

}
