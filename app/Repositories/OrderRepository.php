<?php

namespace App\Repositories;

use App\Hellper\HellperNotifyMessage;
use App\Models\Order;
use App\Models\OrderCard;
use App\Models\Product;
use App\Models\Slider;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class OrderRepository
{
    /**
     * @param $data
     * @param $id
     * @return array
     */
    public function CreateOrUpdate($data, $id)
    {
//        dd($data,$id);
        try {
            DB::beginTransaction();

                $order = Order::query()->updateOrCreate(
                    ['id' => $id],
                    [
                        'user_id' => auth()->id(),
                        'price' => isset($data['shipping']) ? $data['sum'] + $data['shipping'] : $data['sum'],
                        'sale' => null,
                        'shipping' => $data['shipping'],
                        'name' => $data['name'],
                        'city' => $data['city'],
                        'address' => $data['address'],
                        'phone' => $data['phone'],
                        'pay_method' => $data['pay_method'],
                        'location_url' => $data['location_url'],
                        'message' => $data['message'],
                        'status' => 'pending',
                    ]
                );

                foreach ($data['quantity'] as $product_id => $quantity){
                    OrderCard::query()->updateOrCreate(
                        [ 'order_id' => $order->id, 'product_id' => $product_id],
                        [
                            'order_id' => $order->id,
                            'product_id' => $product_id,
                            'quantity' => $quantity
                        ]
                    );
                }
            DB::commit();
            return [
                'message' => HellperNotifyMessage::messageNotify($id),
            ];
        }catch (\Exception $error){
            DB::rollBack();
            return [
                'message' => 'oops',
            ];
        }
    }

}
