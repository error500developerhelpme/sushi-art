<?php

namespace App\Repositories;

use App\Hellper\HellperNotifyMessage;
use App\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryRepository
{
    /**
     * @param $data
     * @param $id
     * @return array
     */
    public function CreateOrUpdate($data, $id)
    {
//        try {
            DB::beginTransaction();
                $category = Category::query()->updateOrCreate(
                    ['id' => $id],
                    [
                        'path' => $data['image'],
                    ]
                );
                foreach ($data['langs'] as $key => $lang){
                    $category->localizations()->updateOrCreate(
                        ['category_id' => $category->id, 'lang' => $key],
                        [
                            'category_id' => $category->id,
                            'lang' => $key,
                            'title' => $lang['title'],
                        ]
                    );
                }
            DB::commit();
            return [
                'message' => HellperNotifyMessage::messageNotify($id),
            ];
//        }catch (\Exception $error){
//            DB::rollBack();
//            return [
//                'message' => 'oops',
//            ];
//        }
    }

}
