<?php

namespace App\Repositories;

use App\Hellper\HellperNotifyMessage;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductRepository
{
    /**
     * @param $data
     * @param $id
     * @return array
     */
    public function CreateOrUpdate($data, $id)
    {
//        try {
            DB::beginTransaction();
                $product = Product::query()->updateOrCreate(
                    ['id' => $id],
                    [
                        'category_id' => $data['category_id'],
                        'price' => $data['price'],
                        'sale' => $data['sale'],
                        'percent' => isset($data['sale']) ? round(100 - (($data['sale'] * 100) / $data['price'])) : null ,
                    ]
                );

                if (!empty($data['images'])){
                    foreach ($data['images'] as $image){
                        $product->images()->create(
                            [
                                'path' => $image,
                            ]
                        );
                    }
                }

                foreach ($data['langs'] as $key => $lang){
                    $product->localizations()->updateOrCreate(
                        ['product_id' => $product->id, 'lang' => $key],
                        [
                            'product_id' => $product->id,
                            'lang' => $key,
                            'title' => $lang['title'],
                            'description' => $lang['description'],
                        ]
                    );
                }
            DB::commit();
            return [
                'message' => HellperNotifyMessage::messageNotify($id),
            ];
//        }catch (\Exception $error){
//            DB::rollBack();
//            return [
//                'message' => 'oops',
//            ];
//        }
    }

}
