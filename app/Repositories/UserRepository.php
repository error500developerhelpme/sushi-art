<?php

namespace App\Repositories;

use App\Hellper\HellperNotifyMessage;
use App\Models\Slider;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    /**
     * @param $data
     * @param $id
     * @return array
     */
    public function CreateOrUpdate($data, $id)
    {
        try {
            DB::beginTransaction();
                User::query()->updateOrCreate(
                    ['id' => $id],
                    [
                        'name' => $data['name'],
                        'type' => 'user',
                        'email' => $data['email'],
                        'password' => Hash::make($data['password']),
                    ]
                );
            DB::commit();
            return [
                'message' => HellperNotifyMessage::messageNotify($id),
            ];
        }catch (\Exception $error){
            DB::rollBack();
            return [
                'message' => 'oops',
            ];
        }
    }

}
