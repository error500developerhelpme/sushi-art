<?php

namespace App\Services;

use App\Hellper\HellperFile;
use App\Repositories\SliderRepository;
use App\Repositories\UserRepository;

class UserService
{
    /**
     * @var UserRepository
     */
    protected UserRepository $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param $data
     * @param null $update
     * @return mixed
     */
    public function CreateOrUpdate($data, $update = null)
    {
        $output = $this->repository->CreateOrUpdate($data, $update);
        return $output;
    }


    public function block($data)
    {
        $block = 1;
        if ($data['block']){
            $block = 0;
        }
        $data->update([
            'block' => $block
        ]);
        return true;
    }

}
