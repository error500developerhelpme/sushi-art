<?php

namespace App\Services;

use App\Hellper\HellperFile;
use App\Repositories\SliderRepository;

class SliderService
{
    /**
     * @var SliderRepository
     */
    protected SliderRepository $repository;

    /**
     * @param SliderRepository $repository
     */
    public function __construct(SliderRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param $data
     * @param null $update
     * @return mixed
     */
    public function CreateOrUpdate($data, $update = null)
    {
        if ($update && isset($data['image'])){
            HellperFile::deleteImage($update['path']);
        }
        if (isset($data['image'])){
            $filePath =  HellperFile::upload($data['image'], '/slider/', 2800);
        }else{
            $filePath = isset($update->path) ? $update->path : null;
        }

        if ($update){
            $update = $update['id'];
        }

        $data['image'] = $filePath;

        $output = $this->repository->CreateOrUpdate($data, $update);
        return $output;
    }

}
