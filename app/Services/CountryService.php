<?php

namespace App\Services;

use App\Hellper\HellperFile;
use App\Hellper\HellperNotifyMessage;
use App\Models\Country;
use App\Repositories\CountryRepository;
use Illuminate\Support\Facades\DB;

class CountryService
{
    /**
     * @var CountryRepository
     */
    protected CountryRepository $repository;

    /**
     * @param CountryRepository $repository
     */
    public function __construct(CountryRepository $repository)
    {
        $this->repository = $repository;
    }

//    /**
//     * @return mixed
//     */
//    public function index(): mixed
//    {
//        return $this->repository->index();
//    }


    /**
     * @param $data
     * @param null $update
     * @return mixed
     */
    public function CreateOrUpdate($data, $update = null)
    {
        if ($update && isset($data['image'])){
            HellperFile::deleteImage($update['path']);
        }
        if (isset($data['image'])){
            $filePath =  HellperFile::upload($data['image'], '/country/', 1024);
        }else{
            $filePath = isset($update->path) ? $update->path : null;
        }

        if ($update){
            $update = $update['id'];
        }else{
            // if you add this new language, the name of the given country does not exist without this
            $data['langs'][$data['lang']] = [
                'name' => $data['name']
            ];
        }
        $data['image'] = $filePath;

        $output = $this->repository->CreateOrUpdate($data, $update);
        return $output;
    }

}
