<?php

namespace App\Services;

use App\Hellper\HellperFile;
use App\Models\Product;
use App\Repositories\OrderRepository;
use App\Repositories\SliderRepository;
use App\Repositories\UserRepository;

class OrderService
{
    /**
     * @var OrderRepository
     */
    protected OrderRepository $repository;

    /**
     * @param OrderRepository $repository
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param $data
     * @param null $update
     * @return mixed
     */
    public function CreateOrUpdate($data, $update = null)
    {
        $data['quantity'] = array_filter($data['quantity']);

        $products = Product::query()->findOrFail(array_keys($data['quantity']));
        $sum = 0;
        foreach ($products as $product){
            $sum += isset($product->sale) ? $product->sale*$data['quantity'][$product->id] : $product->price*$data['quantity'][$product->id];
        }
        $data['sum'] = $sum;
        $output = $this->repository->CreateOrUpdate($data, $update);
        return $output;
    }

}
