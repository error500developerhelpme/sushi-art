<?php

namespace App\Services;

use App\Hellper\HellperFile;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;

class ProductService
{
    /**
     * @var ProductRepository
     */
    protected ProductRepository $repository;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param $data
     * @param null $update
     * @return mixed
     */
    public function CreateOrUpdate($data, $update = null)
    {
        if (!empty($data['images'])){
            foreach ($data['images'] as $image){
                $filePath[] =  HellperFile::upload($image, '/product/', 1024);
            }
        }else{
            $filePath = [];
        }

        if ($update){
            $update = $update['id'];
        }

        $data['images'] = $filePath;

        $output = $this->repository->CreateOrUpdate($data, $update);
        return $output;
    }

}
