<?php

namespace App\Services;

use App\Hellper\HellperFile;
use App\Repositories\CategoryRepository;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    protected CategoryRepository $repository;

    /**
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param $data
     * @param null $update
     * @return mixed
     */
    public function CreateOrUpdate($data, $update = null)
    {
        if ($update && isset($data['image'])){
            HellperFile::deleteImage($update['path']);
        }
        if (isset($data['image'])){
            $filePath =  HellperFile::upload($data['image'], '/category/', 1024);
        }else if(!isset($data['image']) && $update){
            $filePath = isset($update->path) ? $update->path : null;
        }else{
            $filePath = null;
        }

        if ($update){
            $update = $update['id'];
        }

        $data['image'] = $filePath;

        $output = $this->repository->CreateOrUpdate($data, $update);
        return $output;
    }

}
