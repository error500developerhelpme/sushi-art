<?php


namespace App\Http\View;

use App\Models\Country;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class GlobalComposer
{
    public function __construct()
    {
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $page = get_query_page_model('global');
        $view->with('pageGLOBAL', $page);
    }
}
