<?php


namespace App\Http\View;

use App\Models\Country;
use App\Models\ShoppingCard;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ShoppingCardComposer
{
    public function __construct()
    {
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (auth()->user()){
            $data = ShoppingCard::query()->where('user_id', auth()->user()->id)->sum('quantity');
        }else{
            $data = 0;
        }
        $view->with('shoppingCardGLOBAL', $data);
    }
}
