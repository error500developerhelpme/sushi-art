<?php


namespace App\Http\View;

use App\Models\Country;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class OrderComposer
{
    public function __construct()
    {
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = Order::query()->whereNotIn('status', ['finished', 'canceled'])->count();
        $view->with('ordersGLOBAL', $data);
    }
}
