<?php


namespace App\Http\View;

use App\Models\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CountriesComposer
{
    public function __construct()
    {
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = Country::all();
        $view->with('countriesGLOBAL', $data);
    }
}
