<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ShoppingCard;
use Illuminate\Http\Request;

class ShoppingCardController extends Controller
{
    //  working in Ajax all function


    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Product $product)
    {
        $cardAll = ShoppingCard::query()->AuthId();

        $cardProductCount = $cardAll->sum('quantity');
        $cardProduct = $cardAll->where('product_id', $product->id)->first();

        $card = ShoppingCard::query()->updateOrCreate(
            ['user_id' => auth()->user()->id, 'product_id' => $product->id],
            [
                'user_id' => auth()->user()->id,
                'product_id' => $product->id,
                'quantity' => isset($cardProduct->quantity) ? $cardProduct->quantity + 1 : 1,
            ]
        );

        return response()->json($cardProductCount+1 ,200);

    }


    /**
     * @param Request $request
     * @param ShoppingCard $shopping
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, ShoppingCard $shopping)
    {
        $card = $shopping->update(
            [
                'quantity' => $request->quantity,
            ]
        );
        return response()->json($card ,200);
    }


    /**
     * @param ShoppingCard $shopping
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(ShoppingCard $shopping)
    {
        $shopping->delete();
        return response()->json(true ,200);

    }
}
