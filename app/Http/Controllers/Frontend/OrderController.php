<?php

namespace App\Http\Controllers\Frontend;

use App\Hellper\HellperNotifyMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Order\CreateRequest;
use App\Models\Order;
use App\Models\OrderCard;
use App\Models\Product;
use App\Models\ShoppingCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * @param CreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(CreateRequest $request)
    {
        DB::beginTransaction();
            $shoppings = ShoppingCard::query()->AuthId()->with('product')->get();
            $sum = 0;
            foreach ($shoppings as $shopping){
                $sum += isset($shopping->product->sale) ? $shopping->product->sale*$shopping->quantity : $shopping->product->price*$shopping->quantity;
            }

            $order = Order::query()->create([
                'user_id' => auth()->user()->id,
                'price' => $sum,
                'sale' => null,
                'shipping' => 0,
                'name' => $request->name,
                'city' => $request->city,
                'address' => $request->address,
                'phone' => $request->phone,
                'pay_method' => $request->pay_method,
                'location_url' => $request->location_url,
                'message' => $request->message,
                'status' => 'pending',
            ]);

            foreach ($shoppings as $shopping){
                OrderCard::query()->create([
                    'order_id' => $order->id,
                    'product_id' => $shopping->product_id,
                    'quantity' => $shopping->quantity,
                ]);
                $shopping->delete();
            }

            HellperNotifyMessage::smsNotifyTwilio("$sum ֏",  env('TWILIO_OWNER'));

        DB::commit();

        return redirect()->route('home');
    }

    /**
     * @param CreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createAgain(CreateRequest $request)
    {
        $sum = 0;
        foreach ($request->quantity as $key => $item){
            $product = Product::query()->findOrFail($key);
            $sum += isset($product->sale) ? $product->sale*$item : $product->price*$item;
        }

        DB::beginTransaction();
            $orderAgain = Order::query()->create([
                'user_id' => auth()->user()->id,
                'price' => $sum,
                'sale' => null,
                'shipping' => 0,
                'name' => $request->name,
                'city' => $request->city,
                'address' => $request->address,
                'phone' => $request->phone,
                'pay_method' => $request->pay_method,
                'location_url' => $request->location_url,
                'message' => $request->message,
                'status' => 'pending',
            ]);

            foreach ($request->quantity as $product_id => $orderCard){
                OrderCard::query()->create([
                    'order_id' => $orderAgain->id,
                    'product_id' => $product_id,
                    'quantity' => $orderCard,
                ]);
            }

            HellperNotifyMessage::smsNotifyTwilio("$sum ֏",  env('TWILIO_OWNER'));

        DB::commit();

        return redirect()->route('home');
    }
}
