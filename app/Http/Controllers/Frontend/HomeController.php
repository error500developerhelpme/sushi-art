<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ApplicationProposal;
use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductTranslate;
use App\Models\ShoppingCard;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    public function home()
    {
//        $value = Cache::remember('users', $seconds, function () {
//            return DB::table('users')->get();
//        });
        $page = get_query_page_model('home');
        $categories = Category::all();
        $sliders = Slider::query()->GetSlider()->get();
        $products = Product::query()->orderByDesc('id')->limit(3)->get();
        $percent = Product::query()->orderByDesc('percent')->first();
        $percentRandom = Product::query()->whereNotNull('percent')->inRandomOrder()->first();
        return view('frontend.pages.home', compact('sliders', 'products', 'categories', 'page', 'percent', 'percentRandom'));
    }

    public function about()
    {
        return view('frontend.pages.about');
    }

    public function cart()
    {
        $shoppings = ShoppingCard::query()->AuthId()->with('product')->get();
        $sum = 0;
        foreach ($shoppings as $shopping){
            $sum += isset($shopping->product->sale) ? $shopping->product->sale*$shopping->quantity : $shopping->product->price*$shopping->quantity;
        }
        return view('frontend.pages.cart', compact('shoppings', 'sum'));
    }

    public function checkout()
    {
        $shoppings = ShoppingCard::query()->AuthId()->with('product')->get();
        if (count($shoppings) == 0){
            return redirect()->route('shop');
        }
        $sum = 0;
        foreach ($shoppings as $shopping){
            $sum += isset($shopping->product->sale) ? $shopping->product->sale*$shopping->quantity : $shopping->product->price*$shopping->quantity;
        }
        return view('frontend.pages.checkout', compact('shoppings', 'sum'));
    }

    public function contact()
    {
        return view('frontend.pages.contact');
    }

    public function applicationProposal(Request $request)
    {
        ApplicationProposal::query()->create([
            'user_id' => auth()->id() ?? null,
            'title' => $request->title,
            'message' => $request->message,
        ]);
        return view('frontend.pages.contact')->with('success', __('notify.send'));
    }



    public function news()
    {
        return view('frontend.pages.news');
    }

    public function shop()
    {
        $products = Product::all();
        $categories = Category::all();
        return view('frontend.pages.shop', compact('products', 'categories'));
    }

    public function singleNews()
    {
        return view('frontend.pages.single-news');
    }

    public function singleProduct($id)
    {
        $product = Product::query()->findOrFail($id)->load('images', 'category');
        $categoryProduct = Category::query()->findOrFail($product->category->id)->load(['products' => function($q) {
            $q->limit(3);
        }]);
        return view('frontend.pages.single-product', compact('product', 'categoryProduct'));
    }

    public function search(Request $request)
    {
        $products = ProductTranslate::query()->where('title', 'like', '%'.$request->search.'%')->pluck('product_id')->toArray();
        $products = Product::query()->findOrFail($products);
        $categories = Category::all();
        return view('frontend.pages.shop', compact('products', 'categories'));
    }






}
