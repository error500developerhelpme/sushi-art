<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Subscribe\SubscribeRequest;
use App\Http\Requests\Profile\SettingsRequest;
use App\Models\Order;
use App\Models\Subscribe;
use App\Models\User;
use App\Models\UserInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use function Symfony\Component\Routing\Loader\Configurator\collection;

class UserController extends Controller
{
    /**
     * @param SettingsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function settings(SettingsRequest $request): \Illuminate\Http\RedirectResponse
    {
        $user = auth()->user();
        $user->name = $request->name;
        if (isset($request->password) && isset($request->old_password) && Hash::check($request->old_password, $user->password)){
            $user->password = Hash::make($request->password);
        }
        $user->save();

        UserInformation::query()->updateOrCreate(
            ['user_id'=>$user->id],
            [
                'user_id'=>$user->id,
                'phone' => $request->phone,
                'city' => $request->city,
                'address' => $request->address,
            ]
        );

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function profile(): \Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $user = auth()->user();
        $authUser = $user->load(['order' =>function($query){
            $query->with(['card' => function($q){
                $q->with(['product' => function($y) {
                    $y->with('images');
                }]);
            }]); //->whereNot('status', 'finished')
        }]);

        $orderWaiting = $authUser->order;
        $ordersCount = $authUser->order->count();
        $orderHistory = $ordersCount ? $authUser->order->sortByDesc('id')->take(10) : [];

        return view('frontend.pages.profile', compact('orderWaiting','orderHistory', 'ordersCount'));
    }

    /**
     * @param SubscribeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function subscribe(SubscribeRequest $request): \Illuminate\Http\RedirectResponse
    {
        Subscribe::query()->create([
            'email' => $request->email,
        ]);
        return redirect()->back()->with(['success' => __('global.success')]);
    }
}
