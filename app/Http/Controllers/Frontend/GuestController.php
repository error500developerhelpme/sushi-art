<?php

namespace App\Http\Controllers\Frontend;

use App\Hellper\HellperNotifyMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Order\CreateRequest;
use App\Models\Order;
use App\Models\OrderCard;
use App\Models\Product;
use App\Models\ShoppingCard;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class GuestController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse
     */
    public function cart()
    {
        $data = self::guestCookie();
        if (!$data['status']){
            return redirect()->route('shop');
        }
        $shoppings = $data['shoppings'];
        $sum = $data['sum'];
        return view('frontend.pages.cart', compact('shoppings', 'sum'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse
     */
    public function checkout()
    {
        $data = self::guestCookie();
        if (!$data['status']){
            return redirect()->route('shop');
        }
        $shoppings = $data['shoppings'];
        $sum = $data['sum'];

        return view('frontend.pages.checkout', compact('shoppings', 'sum'));
    }

    /**
     * @param CreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrder(CreateRequest $request)
    {
        DB::beginTransaction();
        $data = self::guestCookie();
        if (!$data['status']){
            return redirect()->route('shop');
        }

        $order = Order::query()->create([
            'user_id' => null,
            'price' => $data['sum'],
            'sale' => null,
            'shipping' => 0,
            'name' => $request->name,
            'city' => $request->city,
            'address' => $request->address,
            'phone' => $request->phone,
            'pay_method' => $request->pay_method,
            'location_url' => $request->location_url,
            'message' => $request->message,
            'status' => 'pending',
        ]);

        foreach ($data['shoppings'] as $shopping){
            OrderCard::query()->create([
                'order_id' => $order->id,
                'product_id' => $shopping['product']['id'],
                'quantity' => $shopping['quantity'],
            ]);
        }

            $sum = $data["sum"];
            HellperNotifyMessage::smsNotifyTwilio("$sum ֏", env('TWILIO_OWNER'));

        DB::commit();

        return redirect()->route('home.page')->with('success', __('guest.order_sent'));
    }

    /**
     * @return array|false[]
     */
    public function guestCookie()
    {
        if (!isset($_COOKIE['product_id'])){
            return ['status' => false];
        }
//        dd($_COOKIE['product_id'],$_COOKIE['count'], $_COOKIE);
        $product_ids = array_filter(explode(',', $_COOKIE['product_id']));
        $productFilter = array_count_values($product_ids);
        $shoppings = [];
        foreach ($productFilter as $key => $filter){
//            dd($filter,$productFilter, $_COOKIE['product_id'], $_COOKIE['count']);
            $shoppings[$key] = [
                'id' => null,
                'quantity' => $filter,
                'product' => Product::query()->findOrFail($key)->load('images')
            ];
        }

        $sum = 0;
        foreach ($shoppings as $shopping){
            $sum += isset($shopping['product']['sale']) ? $shopping['product']['sale']*$shopping['quantity'] : $shopping['product']['price']*$shopping['quantity'];
        }

        return [
            'status' => true,
            'shoppings' => $shoppings,
            'sum' => $sum
        ];
    }


}
