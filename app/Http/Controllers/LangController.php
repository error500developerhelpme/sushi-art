<?php

namespace App\Http\Controllers;

use App\Http\Middleware\TranslateMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class LangController extends Controller
{
    /**
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function lang($lang)
    {
        session(['lang' => $lang]);
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed|string
     */
    public static function getLocaleSubmit()
    {
        $get = session('lang');
        if (isset($get)){
            return $get;
        }
        return 'am';
    }
}
