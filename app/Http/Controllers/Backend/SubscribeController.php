<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Subscribe;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\NoReturn;

class SubscribeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        $subscribes = Subscribe::all();
        return view('backend.dashboard.subscribe.index', compact('subscribes'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send($id)
    {
        $subscribes = Subscribe::query()->findOrFail($id);
        dd($subscribes);
        return redirect()->back()->with(['message' => 'Sended']);
    }
}
