<?php

namespace App\Http\Controllers\Backend;

use App\Hellper\HellperFile;
use App\Hellper\HellperNotifyMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\CreateRequest;
use App\Http\Requests\Product\EditRequest;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * @var ProductService
     */
    protected ProductService $product;

    /**
     * @param ProductService $product
     */
    public function __construct(ProductService $product)
    {
        $this->product = $product;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::query()->with('images')->orderByDesc('id')->paginate(10);
        return view('backend.dashboard.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();
        return view('backend.dashboard.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request)
    {
        $data = $this->product->CreateOrUpdate($request->all());
        return redirect()->route('product.index')->with(['message' => $data['message']]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('backend.dashboard.product.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EditRequest $request, Product $product)
    {
        $data = $this->product->CreateOrUpdate($request->all(), $product);
        return redirect()->route('product.index')->with(['message' => $data['message']]);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Product $product): \Illuminate\Http\RedirectResponse
    {
//        HellperFile::deleteImage($product->path);
//        $product->localizations()->delete();
        $product->delete();
        return redirect()->route('product.index')->with(['message' => HellperNotifyMessage::messageNotify('delete')]);
    }

    /**
     * @param Image $image
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOneImage(Image $image)
    {
        HellperFile::deleteImage($image->path);
        $image->delete();
        return response()->json($image->id, 200);
    }


}
