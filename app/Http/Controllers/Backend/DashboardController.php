<?php

namespace App\Http\Controllers\Backend;

use App\Hellper\HellperNotifyMessage;
use App\Http\Controllers\Controller;
use Vonage\Client;
use Vonage\SMS\Message\SMS;

class DashboardController extends Controller
{
    public function index()
    {
        return view('backend.dashboard.dashboard');
    }
}
