<?php

namespace App\Http\Controllers\Backend;

use App\Hellper\HellperFile;
use App\Http\Controllers\Controller;
use App\Http\Requests\Country\CreateRequest;
use App\Http\Requests\Country\EditRequest;
use App\Models\Country;
use App\Services\CountryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function Symfony\Component\Mime\Header\all;

class CountryController extends Controller
{

    /**
     * @var CountryService
     */
    protected CountryService $country;

    /**
     * @param CountryService $country
     */
    public function __construct(CountryService $country)
    {
        $this->country = $country;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $countries = Country::all();
        return view('backend.dashboard.country.index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.dashboard.country.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request)
    {
        $data = $this->country->CreateOrUpdate($request->all());
        return redirect()->route('country.index')->with(['message' => $data['message']]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Country $country)
    {
        return view('backend.dashboard.country.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EditRequest $request, Country $country)
    {
        $data = $this->country->CreateOrUpdate($request->all(), $country);
        return redirect()->route('country.index')->with(['message' => $data['message']]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Country $country)
    {
        HellperFile::deleteImage($country->path);
        $country->localizations()->delete();
        $country->delete();
        return redirect()->route('country.index')->with(['message' => 'Successfully Data deleted!']);
    }
}
