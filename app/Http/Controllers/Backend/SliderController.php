<?php

namespace App\Http\Controllers\Backend;

use App\Hellper\HellperFile;
use App\Http\Controllers\Controller;
use App\Http\Requests\Slider\CreateRequest;
use App\Http\Requests\Slider\EditRequest;
use App\Models\Product;
use App\Models\Slider;
use App\Services\SliderService;
use Illuminate\Http\Request;

class SliderController extends Controller
{

    /**
     * @var SliderService
     */
    protected SliderService $slider;

    /**
     * @param SliderService $slider
     */
    public function __construct(SliderService $slider)
    {
        $this->slider = $slider;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('backend.dashboard.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $products = Product::all();
        return view('backend.dashboard.slider.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request)
    {
        $data = $this->slider->CreateOrUpdate($request->all());
        return redirect()->route('slider.index')->with(['message' => $data['message']]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Slider $slider)
    {
        $products = Product::all();
        return view('backend.dashboard.slider.edit', compact('slider', 'products'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EditRequest $request, Slider $slider)
    {
        $data = $this->slider->CreateOrUpdate($request->all(), $slider);
        return redirect()->route('slider.index')->with(['message' => $data['message']]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Slider $slider)
    {
        HellperFile::deleteImage($slider->path);
        $slider->localizations()->delete();
        $slider->delete();
        return redirect()->route('slider.index')->with(['message' => 'Successfully Data deleted!']);
    }
}
