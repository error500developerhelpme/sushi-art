<?php

namespace App\Http\Controllers\Backend;

use App\Hellper\HellperFile;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CreateRequest;
use App\Http\Requests\Category\EditRequest;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * @var CategoryService
     */
    protected CategoryService $category;

    /**
     * @param CategoryService $category
     */
    public function __construct(CategoryService $category)
    {
        $this->category = $category;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = Category::all();
        return view('backend.dashboard.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.dashboard.category.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request)
    {
        $data = $this->category->CreateOrUpdate($request->all());
        return redirect()->route('category.index')->with(['message' => $data['message']]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        return view('backend.dashboard.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EditRequest $request, Category $category)
    {
        $data = $this->category->CreateOrUpdate($request->all(), $category);
        return redirect()->route('category.index')->with(['message' => $data['message']]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        HellperFile::deleteImage($category->path);
        $category->localizations()->delete();
        $category->delete();
        return redirect()->route('category.index')->with(['message' => 'Successfully Data deleted!']);
    }
}
