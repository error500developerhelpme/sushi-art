<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\SubscribeRequest;
use App\Models\Order;
use App\Models\OrderCard;
use App\Models\Product;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    {
        $orders = Order::query()->whereNotIn('status', ['finished', 'canceled'])->with('card', 'user')->orderByDesc('id')->get();
        return view('backend.dashboard.order.index', compact('orders'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function finished(): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    {
        $orders = Order::query()->whereIn('status', ['finished', 'canceled'])->with('card', 'user')->orderByDesc('id')->paginate(10);
        return view('backend.dashboard.order.finished', compact('orders'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    {
        $products = Product::all();
        return view('backend.dashboard.order.create', compact('products'));
    }

    /**
     * @param SubscribeRequest $request
     * @param OrderService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SubscribeRequest $request, OrderService $service): \Illuminate\Http\RedirectResponse
    {
        $service->CreateOrUpdate($request->all());
        return redirect()->route('order.index');
    }

    /**
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Order $order): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    {
        $order = $order->load(['card' => function($q) {
            $q->with('product');
        }]);
        $orders = Order::query()->where('user_id', $order->user_id)->count();
        return view('backend.dashboard.order.show', compact('order', 'orders'));
    }

    /**
     * @param Order $order
     * @return \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function edit(Order $order): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $products = Product::all();
        $cards = [];
        foreach ($order->card as $card){
            $cards[$card['product_id']] = $card['quantity'];
        }
        return view('backend.dashboard.order.edit', compact('products', 'order', 'cards'));
    }

    /**
     * @param SubscribeRequest $request
     * @param Order $order
     * @param OrderService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SubscribeRequest $request, Order $order, OrderService $service): \Illuminate\Http\RedirectResponse
    {
        $service->CreateOrUpdate($request->all(), $order->id);
        return redirect()->route('order.index');
    }

    /**
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Order $order): \Illuminate\Http\RedirectResponse
    {
        $order->update([
            'status' => 'canceled'
        ]);
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status(Request $request, Order $order): \Illuminate\Http\RedirectResponse
    {
        $order->update([
            'status'=>$request->status,
            'sale'=>$request->sale,
        ]);
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCardOrder(Request $request)
    {
        OrderCard::query()
            ->where('order_id', $request->order_id)
            ->where('product_id', $request->product_id)
            ->first()
            ->delete();

        return response()->json(true, 200);
    }
}
