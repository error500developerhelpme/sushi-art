<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ApplicationProposal;
use Illuminate\Http\Request;

class ApplicationProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $messages = ApplicationProposal::query()->with('user')->paginate(10);
        return view('backend.dashboard.application-proposal.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param ApplicationProposal $applicationProposal
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function show(ApplicationProposal $applicationProposal)
    {
       return view('backend.dashboard.application-proposal.show', compact('applicationProposal'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * @param ApplicationProposal $applicationProposal
     * @return \Illuminate\Http\RedirectResponseasd
     */
    public function destroy(ApplicationProposal $applicationProposal)
    {
        $applicationProposal->delete();
        return redirect()->back()->with(['message' => 'Success']);
    }
}
