<?php

namespace App\Http\Requests\Order;

use App\Hellper\HellperLanguageTrait;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    use HellperLanguageTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'city' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'location_url' => 'nullable',
            'message' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please write name',
            'city.required' => 'Please write city',
            'address.required' => 'Please write address',
            'phone.required' => 'Please write phone',
        ];
    }
}
