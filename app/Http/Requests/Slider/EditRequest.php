<?php

namespace App\Http\Requests\Slider;

use App\Hellper\HellperLanguageTrait;
use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{

    use HellperLanguageTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'array',
            'langs.*' => 'array',
            'langs.'.$this->getFirstLang()['lang'].'.title' => 'required',
            'langs.'.$this->getFirstLang()['lang'].'.sub_title' => 'required',
            'product_id' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'langs.array' => 'Something went wrong',
            'langs.*.array' => 'Something went wrong',
            'langs.'.$this->getFirstLang()['lang'].'.title.required' => 'Please write '.$this->getFirstLang()['name'].' title',
            'langs.'.$this->getFirstLang()['lang'].'.sub_title.required' => 'Please write '.$this->getFirstLang()['name'].' sub title',
        ];
    }
}
