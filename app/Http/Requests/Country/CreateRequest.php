<?php

namespace App\Http\Requests\Country;

use App\Hellper\HellperLanguageTrait;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    use HellperLanguageTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'array',
            'langs.*' => 'array',
            'langs.'.$this->getFirstLang()['lang'].'.name' => 'required|max:255',
            'lang' => 'required|unique:countries,lang|max:255',
            'name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'langs.array' => 'Something went wrong',
            'langs.*.array' => 'Something went wrong',
            'langs.'.$this->getFirstLang()['lang'].'.name.required' => 'Please write '.$this->getFirstLang()['name'].' Name',
            'lang.required' => 'Please write Lang',
            'lang.unique' => 'Please write Lang unique',
            'name.required' => 'Please write Lang unique',
        ];
    }
}
