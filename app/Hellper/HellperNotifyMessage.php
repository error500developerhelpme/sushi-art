<?php


namespace App\Hellper;

use Twilio\Rest\Client;

class HellperNotifyMessage
{
    /**
     * @param $type
     * @return string
     */
    public static function messageNotify($type = null)
    {
        if ($type == 'delete'){
            $messgae = 'Successfully Data Deleted!';
        }elseif ($type){
            $messgae = 'Successfully Data Updated!';
        }else{
            $messgae = 'Successfully Data Created!';
        }
        return $messgae;
    }


    /**
     * @link https://console.twilio.com/?frameUrl=%2Fconsole%3Fx-target-region%3Dus1
     * @param $message
     * @param $receiverNumber
     * @return bool
     */
    public static function smsNotifyTwilio($message = 'test', $receiverNumber = '+37499656558')
    {
        $twilio_sid = env('TWILIO_SID');
        $twilio_token = env('TWILIO_TOKEN');
        $twilio_from = env('TWILIO_FROM');

        try {
            $client = new Client($twilio_sid, $twilio_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_from,
                'body' => $message,
            ]);
           return true;
        }catch (\Exception $err){
            return false;
        }
    }



}
