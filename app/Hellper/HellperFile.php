<?php


namespace App\Hellper;


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as ResizeImage;

class HellperFile
{

    /**
     * @param $file
     * @param $path
     * @param $resize
     * @return string|null
     */
    public static function upload($file, $path, $resizeWidth = false)
    {
        if (!$file) {
            return null;
        }

        // $filePath =  UploadFile::upload($request->image, '/repair/order/', 1024);

        $pathDir = storage_path('app' .DIRECTORY_SEPARATOR. 'public').$path;
        $extension = $file->getClientOriginalExtension();
        $name = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
        $data_name = md5($name.microtime());
        $data = $data_name.'.'.$extension;
        if ($resizeWidth){
            $img = ResizeImage::make($file);
            $img->resize($resizeWidth, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
        }
        $res = $file->move($pathDir, $data);
        $filePath = "/storage". $path . $data;

        if (!$res) {
            return null;
        }

        return $filePath;
    }

    /**
     * @param $path
     * @return bool
     */
    public static function deleteImage($path)
    {

        $PathForDelete = str_replace('/storage', '', $path);
        Storage::delete('/public' . $PathForDelete);
        return true;
    }
}
