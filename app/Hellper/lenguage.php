<?php


use App\Models\Page;

if(!function_exists('get_page_model_translate_value')) {
    /**
     * @info for dynamic text all page
     * @param $data
     * @return mixed
     */
    function get_page_model_translate_value($data)
    {
        if (is_array($data)){
            $lang = session('lang') ?? env('APP_LANG');
            $output = $data[$lang];
        }else{
            $output = $data ?? '';
        }
        return $output;
    }
}


if(!function_exists('get_query_page_model')) {
    /**
     * @param $slug
     * @return mixed
     */
    function get_query_page_model($slug)
    {
        $page = Page::query()->where('slug', $slug)->first();
        $page = json_decode($page->data, true);
        return $page;
    }
}


?>
