<?php

namespace App\Hellper;


use App\Http\Middleware\TranslateMiddleware;
use App\Models\Country;

trait HellperLanguageTrait
{
    /**
     * @param bool $default
     * @return array
     */
    public static function getFirstLang($default = true)
    {
        $lang = Country::query();
        if ($default){
            $lang = $lang->where('lang', env('APP_LANG'))->first();
        }else{
            $lang = $lang->first();
        }

        return  [
            'lang' => $lang->lang,
            'name' => $lang->accesser->name,
        ] ;
    }

    /**
     * @param array $columns
     * @return array
     */
    public static function getAllLangForValidate($columns = [])
    {

        $langs = Country::all();
        $validate = [
            'langs.array' => 'array',
            'langs.*.array' => 'array',
        ];

        foreach ($langs as $key =>$lang) {
            foreach ($columns as  $column => $validateType){
                $validate['langs.' . $lang->lang . '.' . $column] = $validateType ;
            }
        }

        return  $validate;
    }

    /**
     * @param array $columns
     * @return array
     */
    public static function getAllLangForValidateMessage($columns = [])
    {
        $langs = Country::all();
        $validate = [
            'langs.array' => 'Something went wrong',
            'langs.*.array' => 'Something went wrong',
        ];
        foreach ($langs as $key =>$lang) {
            foreach ($columns as $column => $validateType){
                $validate['langs.'.$lang->lang.'.'.$column.'.'.$validateType] = 'Please write '.$lang->name.' '.$column ;
            }
        }

        return  $validate;
    }


}

