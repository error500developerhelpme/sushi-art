<?php


use Illuminate\Support\Facades\Storage;

if(!function_exists('has_file')) {
    /**
     * @info dont isset image, default image random
     * @param $path
     * @param $name
     * @return mixed|string
     */
    function has_file($path, $name)
    {
        $defaultImage = "/assets/frontend/custom/default-$name-image/".rand(1, 11).".png";
        if (isset($path) && file_exists(public_path($path))){
            $defaultImage = $path;
        }
        return $defaultImage;
    }
}


?>
