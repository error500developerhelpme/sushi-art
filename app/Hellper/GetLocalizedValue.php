<?php


namespace App\Hellper;


use App\Http\Middleware\TranslateMiddleware;
use App\Models\Country;

class GetLocalizedValue
{
    /**
     * @param $model
     * @param $column
     * @return mixed|null
     */
    public static function GetValue($model, $column)
    {
        if (isset($model->accesser->$column)){
            return $model->accesser->$column;
        }else{
            return self::getDefLangVal($model, $column);
        }
    }

    public static function GetSelectLang($lang)
    {
        $country = Country::query()->where('lang', $lang)->first();
        return [
            'accesser'=>$country->accesser,
            'country'=>$country,
        ];
    }


    /**
     * @param $model
     * @param $column
     * @param null $id
     * @param null $lang
     * @return null
     */
    public static function getDefLangVal($model, $column, $id = null, $lang = null)
    {
        if($model){
            if (is_string($model)){
                $modelPath = 'App\Models\\'.$model::findOrFail($id);

            }else{
                $modelPath = $model;
            }

            if (!$lang){
                $mainLanguage = env('APP_LANG');
            }else{
                $mainLanguage = $lang;
            }

            $item = $modelPath->load(['localizations' => function($query) use($mainLanguage){
                $query->where('lang', $mainLanguage);
            }]);

            $item = $item->localizations->first();

            if($item) return $item->$column;
        }

        return null;
    }

}
