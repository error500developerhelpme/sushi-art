<?php

use App\Http\Controllers\Backend\AdditionalController;
use App\Http\Controllers\Backend\ApplicationProposalController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\CountryController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\SliderController;
use App\Http\Controllers\Backend\SubscribeController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\LangController;
use App\Http\Middleware\TranslateMiddleware;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

// ------------------------------------------- BackEnd ------------------------------------------- \\
Route::group(['middleware'=> ['auth', 'admin'], 'prefix' => '/admin'], function() {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('/country', CountryController::class);
    Route::resource('/user', UserController::class);
    Route::resource('/slider', SliderController::class);
    Route::resource('/category', CategoryController::class);
    Route::resource('/product', ProductController::class);
    Route::get('/product/delete/image/{image}', [ProductController::class, 'deleteOneImage'])->name('product.delete.image');
    Route::resource('/order', OrderController::class);
    Route::post('/order/card/delete', [OrderController::class, 'deleteCardOrder'])->name('order.card.delete');
    Route::get('/order/finish/get', [OrderController::class, 'finished'])->name('order.finished');
    Route::post('/order/status/{order}', [OrderController::class, 'status'])->name('order.status');
    Route::resource('/application-proposal', ApplicationProposalController::class);

    Route::get('/subscribe', [SubscribeController::class, 'index'])->name('subscribe.index');
    Route::get('/subscribe/send/{id}', [SubscribeController::class, 'send'])->name('subscribe.send');
});
// ---------------------------------------- [END] BackEnd ---------------------------------------- \\




// ------------------------------------------- FrontEnd ------------------------------------------- \\

// change language
Route::get('setlocale/{lang}/', [LangController::class,'lang'])->name('setlocale');


//Route::group( ['prefix' => LangController::getLocaleSubmit(), 'namespace' => 'App\Http\Controllers\Frontend'], function() {
Route::group( ['namespace' => 'App\Http\Controllers\Frontend'], function() {
    Route::get('/', 'HomeController@home')->name('home.page');
    Route::get('/about', 'HomeController@about')->name('about');
    Route::get('/contact', 'HomeController@contact')->name('contact');
    Route::post('/contact/send', 'HomeController@applicationProposal')->name('contact.application.proposal');
    Route::get('/news', 'HomeController@news')->name('news');
    Route::get('/shop', 'HomeController@shop')->name('shop');
    Route::get('/singleNews', 'HomeController@singleNews')->name('single.news');
    Route::get('/singleProduct/{id}', 'HomeController@singleProduct')->name('single.product');
    Route::post('/search', 'HomeController@search')->name('search');
    Route::post('/subscribe', 'UserController@subscribe')->name('subscribe');



    Route::group(['prefix' => '/guest', 'middleware'=> ['guest']], function() {
        Route::get('/cart', 'GuestController@cart')->name('guest.cart');
        Route::get('/checkout', 'GuestController@checkout')->name('guest.checkout');
        Route::post('/order', 'GuestController@createOrder')->name('guest.order');
    });

    Route::group(['middleware'=> ['auth']], function() {
        Route::get('/home', 'UserController@profile')->name('home');
        Route::post('/change/settings', 'UserController@settings')->name('settings.user');
        Route::get('/cart', 'HomeController@cart')->name('cart');
        Route::get('/checkout', 'HomeController@checkout')->name('checkout');

        Route::group(['middleware'=> ['is_block']], function() {
            Route::group(['prefix' => '/order'], function () {
                Route::post('/', 'OrderController@create')->name('order');
                Route::post('/again', 'OrderController@createAgain')->name('order.again');
            });

            Route::group(['prefix' => '/card'], function () {
                Route::get('/add/{product}', 'ShoppingCardController@create')->name('shopping.card.create');
                Route::post('/update/{shopping}', 'ShoppingCardController@update')->name('shopping.card.update');
                Route::get('/delete/{shopping}', 'ShoppingCardController@delete')->name('shopping.card.delete');
            });
        });
    });

});

// ---------------------------------------- [END] FrontEnd ---------------------------------------- \\
