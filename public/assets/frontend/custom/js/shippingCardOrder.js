// --------------------- guest card count --------------------- \\
$( document ).ready(function countGuestProduct() {
    let resetCookie = $.cookie('reset_cookie');
    if (!resetCookie){
        $.cookie('reset_cookie', 'true', { path: '/' });
        $.cookie('count', 0, { path: '/' })
        $.cookie('product_id', '', { path: '/' })
    }
    let productCount = $.cookie('count');
    if (!productCount){
        $('.style-shipping-card-count').attr('style', 'display:none')
    }
    $('.cookie-count-product').html(productCount);
});


// --------------------- add to card --------------------- \\
$(document).on('click', '.javas-add-to-card', function() {
    let id = $(this).attr('data-id');
    let userId = $(this).attr('data-user');
    if (userId.length){
        $.ajax({
            type: 'get',
            url: '/card/add/'+id,
            dataType: 'json',
            success: function (res) {
                navBarCount(res)
            },
            error: function (data) {
                alert("OOPS ERROR");
            }
        });
    }else{
        // TODO guest
        let productId = $.cookie('product_id');
        let productCount = $.cookie('count') ?? 0;
        let plusCount = (productCount*1)+1;
        $.cookie('product_id', `${id},${productId}`);
        $.cookie('count', plusCount);


        navBarCount(plusCount)

    }
});


// --------------------- remove in card --------------------- \\
$(document).on('click', '.remove-shopping-card', function() {
    let id = $(this).attr('data-id');
    let price = $(this).attr('data-price');
    let sum = $('.shopping-card-subtotal').text();
    let access =  $(this).attr('data-access');
    let quantity = $(this).closest('.table-body-row').find('.product-quantity').find('.shopping-card-quantity').val();
    let total =  sum - (price*quantity);
    let userID =  $(this).attr('data-user'); // guest or login user
    let productID =  $(this).attr('data-product'); // guest
    let cookieCount = $.cookie('count'); // guest

    if (userID){
        if (access === 'true') {
            $.ajax({
                type: 'get',
                url: '/card/delete/' + id,
                dataType: 'json',
                success: (res) => {
                    if (res) {
                        $(this).closest('.table-body-row').remove()
                        totalPrice(total)

                        // quantity count in navbar
                        let quan = $('.shopping-card-quantity');
                        let quantityTotal = 0;
                        for (let i = 0; i < quan.length; i++) {
                            quantityTotal += quan[i].value << 0;
                        }

                        navBarCount(quantityTotal)
                        // $('.style-shipping-card-count').html(quantityTotal)
                    }
                },
                error: function (data) {
                    alert("OOPS ERROR");
                }
            });
        }else{
            $(this).closest('.table-body-row').remove()
            totalPrice(total)
        }
    }
    else{
        // TODO guest
        let count = $.cookie('count');
        let getProductId = $.cookie('product_id')

        $(this).closest('.table-body-row').remove()
        navBarCount(count - quantity)
        totalPrice(total)

        $.cookie('count', cookieCount - quantity,  { path: '/' });


        // Remove trailing comma if present
        let inputString = getProductId.replace(/,\s*$/, "");

        // Convert string to array
        let numberArray = $.map(inputString.split(','), function(value) {
            return parseInt(value, 10);
        });

        // Remove all occurrences of the value from the array
        numberArray = numberArray.filter(function(value) {
            return value !== parseInt(productID);
        });

        // Convert the array back to a string
        let newStringCookie = numberArray.join(',');

        $.cookie('product_id', newStringCookie, { path: '/' })
    }

});


// --------------------- change/update/change in card --------------------- \\
$(document).on('change', '.shopping-card-quantity', function() {
    if ($(this).val() < 1){
        $(this).val('1')
    }
    let id = $(this).attr('data-id');
    let price = $(this).attr('data-price');
    let priceAll = $('.shopping-card-subtotal').attr('data-sum');
    let access =  $(this).attr('data-access');
    let userID =  $(this).attr('data-user');
    let dataProductId =  $(this).attr('data-product');
    let dataQuantity =  $(this).attr('data-quantity');


    // in one card total
    $(this).closest('.table-body-row').find('.product-sum-total').find('.all-price-total').html(price*$(this).val() + ' ')

    // total price sum
    let len = $('.all-price-total').text().split(' ');
    let total = 0;
    for (let i = 0; i < len.length; i++) {
        total += len[i] << 0;
    }

    // quantity count in navbar
    let quan = $('.shopping-card-quantity');
    let quantityTotal = 0;
    for (let i = 0; i < quan.length; i++) {
        quantityTotal += quan[i].value << 0;
    }

    let data = {
        'quantity': $(this).val(),
    };

    if (userID) {
        if (access === 'true') {
            $.ajax({
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/card/update/' + id,
                data: data,
                dataType: 'json',
                success: function (res) {
                    console.log(res)
                    navBarCount(quantityTotal)
                    $('.shopping-card-subtotal').attr('data-sum', total);
                    totalPrice(total)
                },
                error: function (data) {
                    alert("OOPS ERROR");
                }
            });
        } else {
            $('.shopping-card-subtotal').attr('data-sum', total);
            totalPrice(total)
        }
    }
    else{
        // TODO guest
        let productIdsCookie = $.cookie('product_id');
        let productCount = $.cookie('count')*1 ?? 0;
        let outputCookieProductIds = '';
        let outputCookieCount = 0;

        let currentValue = parseFloat($(this).val());

        if (!isNaN(currentValue)) {
            let previousValue = !isNaN(parseFloat($(this).data("previous-value"))) ? parseFloat($(this).data("previous-value")) : dataQuantity;

            if (currentValue > previousValue) { // todo '+'

                outputCookieProductIds = `${dataProductId},${productIdsCookie}`;
                outputCookieCount = productCount+1

            } else if (currentValue < previousValue) { // todo '-'

                // Remove trailing comma if present
                let inputString = productIdsCookie.replace(/,\s*$/, "");

                // Convert string to array, "Original Array output map:", numberArray
                let numberArray = $.map(inputString.split(','), function(value) {
                    return parseInt(value, 10);
                });

                // Remove the first occurrence of the element from the array
                let index = numberArray.indexOf(parseInt(dataProductId));

                if (index !== -1) {
                    numberArray.splice(index, 1);
                }


                outputCookieProductIds = `${numberArray.toString()}`;
                outputCookieCount = productCount-1;
            }

            // Обновляем предыдущее значение в data
            $(this).data("previous-value", currentValue);
        }




        $.cookie('product_id', outputCookieProductIds, { path: '/' });
        $.cookie('count', outputCookieCount, { path: '/' });

        navBarCount(outputCookieCount)

        $('.shopping-card-subtotal').attr('data-sum', total);
        totalPrice(total)
    }
});



// --------------------- create order in checkout --------------------- \\
$(document).on('click', '.create-order-checkout', function() {
   $('.checkout-form-submit').submit();
});

// --------------------- functions --------------------- \\

function navBarCount(params) {
    $('.style-shipping-card-count').attr('style', 'font-size:15px')
    $('.style-shipping-card-count').html(params)
    setTimeout(()=>{
        $('.style-shipping-card-count').attr('style', 'font-size:14px')
    }, 300)
    setTimeout(()=>{
        $('.style-shipping-card-count').attr('style', 'font-size:13px')
    }, 400)
    setTimeout(()=>{
        $('.style-shipping-card-count').attr('style', 'font-size:12px')
    }, 500)
}


function totalPrice(params) {
    $('.shopping-card-subtotal').html(params)
    $('.shopping-card-shipping').html(params > 5000 ? 0 : '~//~' )
    $('.shopping-card-total').html(params)
}
