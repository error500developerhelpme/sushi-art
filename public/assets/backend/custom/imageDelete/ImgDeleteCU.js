/**
 *  how to run
 *  in html you cen add meny classes
 *  <div class="file-drel"></div>
 *
 *  please add this html wher you wont and call in down this js file FileDrel function
 *
 *  if do you wont one uploader file plesa add in down script where called function FileDrel
 *  add here one object with parametr, do you wont two uploder add 2 object
 *
 *  contacts
 *  david656558a@gmail.com
 *
 *  autor
 *  David Kirakosyan
 */


/**
 * for example
 *

 <!doctype html>
 <html>
 <head>
  <meta charset="utf-8">
  <title>jQuery Plugin</title>
 </head>
  <body>
   <div class="file-drel"></div>
   <div class="file-drel"></div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
   <script src="ImgDeleteCU.js"></script>
   <script>
     FileDrel(
            [
                 {
                    name: 'drel',
                    multiple: true,
                    label: 'Upload a file',
                    // option: ['video', 'image', 'audio']
                 },
                 {
                    name: 'drel1',
                    multiple: true,
                    label: 'Upload a file111',
                 }
            ]
     )
   </script>
  </body>
 </html>

 */


// =================================================== D.K. ===========================================================

var filesDataGlobal = [];

/**
 *  start function
 */
function FileDrel(parametr)
{
    let className =  $('.file-drel')
    $.each(className, function(key, value) {

        let multipleData = parametr[key].multiple ? 'multiple' : ''
        let nameData = parametr[key].name
        let labelData = parametr[key].label

        if (!nameData || !labelData){
            return false
        }

        filesDataGlobal[key] = []
        $(value).html(`
            <div class="file-drel-main-div">
                <div class="upload-btn-wrapper">
                    <button class="btn">${labelData}</button>
                    <input type="file" class="file-div-input" data-index="${key}" name="${nameData}" ${multipleData}/>
                </div>
                <div class="file-drel-view"></div>
            </div>
        `)
    })
}


/**
 *  main functionality for view
 */
$(document).ready(function ($) {

    /**
     *  input change functionality
     */
    $(document).on('change', '.file-div-input', function (e) {
        FileDrelrender($(this), true);
    })

    /**
     *  remove files functionality
     */

    $(document).on('click', '.file-drel-remove', function () {
        let removeIndex = Number($(this).attr('data-value'));
        let inputFile = $(this).closest('.file-drel-main-div').find('.file-div-input');
        let indexInput = inputFile.attr('data-index')
        // ======= in global array removed item on which you have clicked on using index ======= //
        filesDataGlobal[indexInput].splice(removeIndex, 1)

        // ======= collect all the elements that are left on the global array ======= //
        let dt = new DataTransfer();
        for (let i=0; i<filesDataGlobal[indexInput].length; i++){
            dt.items.add(filesDataGlobal[indexInput][i]);
        }

        // ======= add other variables to input core files ======= //
        inputFile[0].files = dt.files;
        FileDrelrender(inputFile, false);
    })

    /**
     *  html appendet functionality
     */
    function FileDrelrender(file, merge = true, type = null){
        let el = file.closest('.file-drel-main-div').find('.file-drel-view');
        if (!el.length){
            console.log('Please add this class in main div file-drel-main-div')
        }
        el.html(``);

        let indexInput = file.attr('data-index')
        // ======= if this add or edit files "merge true" if this remove file "merge false" ======= //
        // ======= in order note mixe date ======= //
        if (merge){
            $.merge(filesDataGlobal[indexInput], file[0].files)
            let dt = new DataTransfer();
            for (let i=0; i < filesDataGlobal[indexInput].length; i++){
                dt.items.add(filesDataGlobal[indexInput][i]);
            }
            file[0].files = dt.files
        }


        // ======= added html ======= //
        for (let i=0; i < filesDataGlobal[indexInput].length; i++){

            console.log(file)

            let src = URL.createObjectURL(filesDataGlobal[indexInput][i])

            let dataFile = `<div>OOPS</div>`;
            let addClassStructur = '';
            let addStyleStructur = '';
            let addVideNameCountStructur = 15;
            if(type === 'image' || filesDataGlobal[indexInput][i].type.split('/')[0] === 'image'){
                addClassStructur = 'file-drel-view-file-for-image'
                dataFile = `<img style="width: 100%; object-fit: cover;" src="${src}">`
            }
            else if(type === 'video' || filesDataGlobal[indexInput][i].type.split('/')[0] === 'video'){
                addVideNameCountStructur = 30
                dataFile = `
                    <video width="400" controls>
                        <source src="${src}" type="video/mp4">
                        <source src="${src}" type="video/ogg">
                        Your browser does not support HTML video.
                    </video>
            `
            }
            else if(type === 'audio' || filesDataGlobal[indexInput][i].type.split('/')[0] === 'audio'){
                addStyleStructur = 'style="display:none"'
                dataFile = `
                <audio controls>
                    <source src="${src}" type="audio/ogg">
                    <source src="${src}" type="audio/mpeg">
                </audio>
            `
            }



            el.append(`
                    <div class="file-drel-view-file ${addClassStructur}" >
                         <div class="file-drel-hover" >
                             <div class="file-drel-menu" data-value="${i}" ${addStyleStructur}><img class="file-drel-remove-icon" src="menu.png" alt=""></div>
                             <div class="file-drel-remove" data-value="${i}" ><img class="file-drel-remove-icon" src="delete-icon-14.png" alt=""></div>
                             <div class="file-drel-menu-hover">
                                 <div data-value="${i}" class="file-drel-size"> size: ${filesDataGlobal[indexInput][i].size/1000}kb. </div>
                                 <div data-value="${i}" class="file-drel-name"> ${filesDataGlobal[indexInput][i].name.length > addVideNameCountStructur ? filesDataGlobal[indexInput][i].name.slice(0,addVideNameCountStructur) + '...' : filesDataGlobal[indexInput][i].name} </div>
                             </div>
                         </div>
                         ${dataFile}
                    </div>
                    <br>
                `);
        }
    }



    $(document).on('click', '.file-drel-menu', function () {
        if ($(this).closest('.file-drel-hover').find('.file-drel-menu-hover').attr('style')){
            $(this).closest('.file-drel-hover').find('.file-drel-menu-hover').removeAttr('style')
            $(this).closest('.file-drel-hover').removeAttr('style')
        }else{
            $(this).closest('.file-drel-hover').find('.file-drel-menu-hover').attr('style', 'display:block')
            $(this).closest('.file-drel-hover').attr('style', 'z-index: 1')
        }
    })



})


/**
 * css for view
 */
$(document).ready(function (){
    $('head').append(`
    <style>

        /*.......  button input .......*/
        .upload-btn-wrapper {
            position: relative;
            overflow: hidden;
            display: inline-block;
        }

        .btn {
            border: 2px solid gray;
            color: gray;
            background-color: white;
            padding: 8px 20px;
            border-radius: 8px;
            font-size: 20px;
            font-weight: bold;
        }

        .upload-btn-wrapper input[type=file] {
            font-size: 100px;
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0;
        }
        /*.......  [END] button input .......*/

        /*.......  style view image .......*/
        .file-drel-hover{
            height: 100%;
            width: 100%;
            position: absolute;
            display: none;
        }

        .file-drel-view{
            display: flex;
            flex-wrap: wrap;
        }
        .file-drel-view-file-for-image{
            width: 200px;
            height: 200px;
        }
        .file-drel-view-file{
            display:flex;
            float: left;
            margin: 10px;
            position:relative;

        }
        .file-drel-view-file:hover > .file-drel-hover{
            display: block;

        }

        .file-drel-menu{
            position:absolute;
            top: 5px;
            left: 5px;
            cursor: pointer;
            width: 29px;
            height: 29px;
            border-radius: 50px ;
            z-index: 1;
        }

        .file-drel-menu-hover{
            display: none;
            background: rgba(221, 221, 221, 0.43);
            height: 100%;
            width: 100%;
        }

        .file-drel-remove{
            position:absolute;
            top: 5px;
            right: 5px;
            cursor: pointer;
            width: 29px;
            height: 29px;
            border-radius: 50px ;
            z-index: 1;
        }
        .file-drel-remove-icon{
            width: 30px;
        }
        .file-drel-size{
            font-size: 19px;
            color: black;
            position: absolute;
            bottom: 60%;
            top: auto;
            left: 7%;
            font-family: fantasy;
            right: auto;
        }
        .file-drel-name{
            font-size: 19px;
            color: black;
            position: absolute;
            bottom: 50%;
            top: auto;
            left: 7%;
            font-family: fantasy;
            right: auto;
        }
        /*.......  [END] style view image .......*/
    </style>
    `)
})





