$(document).on('click', '.del-clic' ,function () {
    var id = $(this).attr('data-id');
    $.ajax({
        type: 'GET',
        url: "/admin/product/delete/image/" + id,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name = "csrf-token"]').attr('content')
        },
        success: function(result){
            console.log(result);
            $('.image[data-id="'+ result +'"]').remove();
        }
    });
})


