<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryTranslate;
use App\Models\Country;
use App\Models\CountryTranslate;
use App\Models\Slider;
use App\Models\SliderTranslate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SliderSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $array = [
            [
                'path' => 'assets/custom/img/IMG_2291.jpg',
                'translate' => [
                    [
                        'title' => 'Սեթեր',
                        'sub_title' => 'Սեթեր',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Сеты',
                        'sub_title' => 'Сеты',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Sets',
                        'sub_title' => 'Sets',
                        'lang' => 'en',
                    ],
                ]
            ],
            [
                'path' => 'assets/custom/img/IMG_2293.jpg',
                'translate' => [
                    [
                        'title' => 'Բում առաջարկ',
                        'sub_title' => 'Բում առաջարկ',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Гарячий предложение',
                        'sub_title' => 'Гарячий предложение',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Hot offer',
                        'sub_title' => 'Hot offer',
                        'lang' => 'en',
                    ],
                ]
            ],
            [
                'path' => 'assets/custom/img/IMG_2302.jpg',
                'translate' => [
                    [
                        'title' => 'Նորույթ',
                        'sub_title' => 'Նորույթ',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Новинка',
                        'sub_title' => 'Новинка',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Latest',
                        'sub_title' => 'Latest',
                        'lang' => 'en',
                    ],
                ]
            ],

        ];


        foreach ($array as $val){
            $item = Slider::query()->create([
                'path' => $val['path'],
            ]);

            foreach ($val['translate'] as $translate){
                    SliderTranslate::query()->create([
                        'slider_id' => $item->id,
                        'lang' => $translate['lang'],
                        'title' => $translate['title'],
                        'sub_title' => $translate['sub_title'],
                    ]);
            }
        }



    }
}
