<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserInfo;
use DB;
use Hash;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $admin = User::create([
            'name' => 'admin',
            'type' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
//            'password' => Hash::make('Artursushiadmin199'),
            'block' => 0,
        ]);

        $support = User::create([
            'name' => 'Support',
            'type' => 'support',
            'email' => 'support@gmail.com',
            'password' => Hash::make('password'),
            'block' => 0,
        ]);

        $user = User::create([
            'name' => 'user',
            'type' => 'user',
            'email' => 'user@gmail.com',
            'password' => Hash::make('password'),
            'block' => 0,
        ]);
    }
}
