<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryTranslate;
use App\Models\Country;
use App\Models\CountryTranslate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            [
                'path' => null,
                'translate' => [
                    [
                        'title' => 'Սեթեր',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Сеты',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Sets',
                        'lang' => 'en',
                    ],
                ]
            ],
            [
                'path' => null,
                'translate' => [
                    [
                        'title' => 'Բում առաջարկ',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Гарячий предложение',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Hot offer',
                        'lang' => 'en',
                    ],
                ]
            ],
            [
                'path' => null,
                'translate' => [
                    [
                        'title' => 'Նորույթ',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Новинка',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Latest',
                        'lang' => 'en',
                    ],
                ]
            ],

        ];


        foreach ($categories as $category){
            $categoryGlob = Category::query()->create([
                'path' => $category['path'],
            ]);

            foreach ($category['translate'] as $translate){
                    CategoryTranslate::query()->create([
                        'category_id' => $categoryGlob->id,
                        'lang' => $translate['lang'],
                        'title' => $translate['title'],
                    ]);
            }
        }



    }
}
