<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\CountryTranslate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CountySeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $countries = [
            [
                'lang' => 'am',
                'path' => '/assets/custom/countries/armenia.svg',
                'translate' => [
                    [
                        'name' => 'Հայրեն',
                        'lang' => 'am',
                    ],
                    [
                        'name' => 'Армянский',
                        'lang' => 'ru',
                    ],
                    [
                        'name' => 'Armenia',
                        'lang' => 'en',
                    ],
                ]
            ],
            [
                'lang' => 'ru',
                'path' => '/assets/custom/countries/russia.svg',
                'translate' => [
                    [
                        'name' => 'Ռուսերեն',
                        'lang' => 'am',
                    ],
                    [
                        'name' => 'Русский',
                        'lang' => 'ru',
                    ],
                    [
                        'name' => 'Russia',
                        'lang' => 'en',
                    ],
                ]
            ],
            [
                'lang' => 'en',
                'path' => '/assets/custom/countries/us.png',
                'translate' => [
                    [
                        'name' => 'Անգլերեն',
                        'lang' => 'am',
                    ],
                    [
                        'name' => 'Англиский',
                        'lang' => 'ru',
                    ],

                    [
                        'name' => 'English',
                        'lang' => 'en',
                    ],
                ]
            ],
        ];


        foreach ($countries as $country){
            $countryGlobal = Country::query()->create([
                'lang' => $country['lang'],
                'path' => $country['path'],
            ]);

            foreach ($country['translate'] as $translate){
                    CountryTranslate::query()->create([
                        'country_id' => $countryGlobal->id,
                        'lang' => $translate['lang'],
                        'name' => $translate['name'],
                    ]);
            }
        }



    }
}
