<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryTranslate;
use App\Models\Country;
use App\Models\CountryTranslate;
use App\Models\Page;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class PagesSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $pages = [
            'home' => [
                //cervices
                'shipping' => [
                    'title' => [
                        'am' => 'Անվճար առաքում',
                        'ru' => 'Бесплатная доставка',
                        'en' => 'Free Shipping',
                    ],
                    'description' => [
                        'am' => '5000 և ավելի պատվերների դեպքում',
                        'ru' => 'При заказе от 5000 и более',
                        'en' => 'For orders of 5000 or more',
                    ],
                ],
                'support' => [
                    'title' => [
                        'am' => '11:00/00:00',
                        'ru' => '11:00/00:00',
                        'en' => '11:00/00:00',
                    ],
                    'description' => [
                        'am' => 'Ստացեք աջակցություն ամբողջ օրը',
                        'ru' => 'Получите поддержку круглосуточно',
                        'en' => 'Get support around the clock',
                    ],
                ],
                'refund' => [
                    'title' => [
                        'am' => 'Ընտրիր քեզ հարմար Վճարման եղանակ',
                        'ru' => 'Выберите способ оплаты, который вам подходит',
                        'en' => 'Choose the payment method that suits you',
                    ],
                    'description' => [
                        'am' => 'Հաշվեհամար,առ ձեռն,IDRAM, Telcell',
                        'ru' => 'Номер счета, вручную, IDRAM, Telcell',
                        'en' => 'Bank card number, Cash, IDRAM, Telcell',
                    ],
                ],

                //offer
                'section_offer_month' => [
                    'title_black' => [
                        'am'=>'առաջարկ',
                        'ru'=>'месяца',
                        'en'=>'month',
                    ],
                    'title_orange' => [
                        'am' => 'Ամսվա',
                        'ru' => 'Предложение',
                        'en' => 'Offer of the',
                    ],
                    'sub_title' => [
                        'am' => 'Փոփոխվող տեսականի',
                        'ru' => 'Изменение диапазона',
                        'en' => 'Changing range',
                    ],
                    'description' => [
                        'am' => 'Հարգելի հաճախորդներ մենք փորձում ենք անել ամեն հնարավորը, որպեսզի մեր ծառայությունը Ձեզ համար առավել հարմարավետ լինի:',
                        'ru' => 'Уважаемые клиенты, мы стараемся сделать все возможное, чтобы сделать наш сервис более удобным для вас.',
                        'en' => 'Dear customers, we try to do everything possible to make our service more convenient for you.',
                    ],
                    'timer' => 0,
                ],

                //video
                'section_video' => [
                    'image' => '/assets/custom/img/sushi/36.png',
                    'video' => '/assets/frontend/custom/video/video-home-page.MP4',
                    'title_black' => [
                        'am' => 'Փոքրիկ',
                        'ru' => 'Немного',
                        'en' => 'A little',
                    ],
                    'title_orange' => [
                        'am' => 'տեղեկատվություն',
                        'ru' => 'информации',
                        'en' => 'information',
                    ],
                    'sub_title' => [
                        'am' => '',
                        'ru' => '',
                        'en' => '',
                    ],
                    'description' => [
                        'am' => 'Գիտե՞իր, որ սուշին և ռոլլերը լավագույն միջոցներից են նիհարելու համար: Այն չափազանց օգտակար է և ունի քիչ կալորիականություն:',
                        'ru' => 'Знаете ли вы, что суши и роллы — один из лучших способов похудеть? Он чрезвычайно полезен и имеет мало калорий.',
                        'en' => 'Did you know that sushi and rolls are one of the best ways to lose weight? It is extremely useful and has few calories.',
                    ],
                    'button' => 0
                ],

                //sale
                'section_sale' => [
                    'image' => '/assets/custom/img/fon-fotor.jpg',
                    'title_black' => [
                        'am' => 'Ամենամեծ զեղջը մեզ մոտ! <br> համեցեք',
                        'ru' => 'ДСамая большая скидка у нас! <br> с Наслаждайтесь',
                        'en' => 'The biggest discount with us! <br> Enjoy',
                    ],
                    'title_orange' => [
                        'am' => 'Զեղչ և ոչ միայն...',
                        'ru' => 'Скидка и не только...',
                        'en' => 'Discount and not only...',
                    ],
                ],

            ],
            'contacts' => [
                'contact_text' => [
                    'title' => [
                        'am' => 'Ձեր կարծիքը/բողոքը/առաջարկը',
                        'ru' => 'Ваш отзыв/жалоба/предложение',
                        'en' => 'Your feedback/complaint/suggestion',
                    ],
                    'description1' => [
                        'am' => 'Ձեր կողմից գրված կարծիքը անանուն է կարող եք գրել Ձեր կարքիը առանց քաշվելու:',
                        'ru' => 'Написанное вами мнение анонимно, вы можете написать свое мнение.',
                        'en' => 'The opinion you write is anonymous, you can write your opinion.',
                    ],
                    'description2' => [
                        'am' => 'Դուք կարող եք գրել Ձեր կարծիքը կամ բողոքը կամ առաջարկը կայքի և ոչ միայն, նաև առաքման կամ ուտելիքի որակի հետ կապված, տվյալ նամակը կարդում է անձամփ տնորենը, Ձեր կարծիքը շատ կարևոր է մեզ և մեր կայքի բարելավման համար, Շնորհակալություն սիրով, SushiArt թիմ։',
                        'ru' => 'Вы можете написать свое мнение или жалобу или предложение по поводу сайта и не только, также касающееся доставки или качества еды, данное письмо читает владелец, Ваше мнение очень важно для нас и для улучшения нашего сайта, Спасибо, команда SushiArt.',
                        'en' => 'You can write your opinion or complaint or suggestion about the site and not only, also related to delivery or food quality, the given letter is read by the owner, your opinion is very important for us and for the improvement of our site, Thank you very much, SushiArt team.',
                    ],
                ],

            ],
            'global' => [
                'about' => [
                    'description' => [
                        'am' => 'Հարգելի հաճախորդներ մենք փորձում ենք անել ամեն հնարավորը, որպեսզի մեր ծառայությունը Ձեզ համար առավել հարմարավետ լինի:',
                        'ru' => 'Уважаемые клиенты, мы стараемся сделать все возможное, чтобы сделать наш сервис более удобным для вас.',
                        'en' => 'Dear customers, we try to do everything possible to make our service more convenient for you.',
                    ],
                ],
                'contact_info' => [
                    'shop_address' => [
                        'am' => 'Երևան',
                        'ru' => 'Ереван',
                        'en' => 'Yerevan',
                    ],
                    'shop_hours' => [
                        'am' => 'ԵՐԿԲ - ՈՒՐԲԱԹ: 08։00֊21։00 <br> ՇԱԲ - ԿԻՐ: 10։00֊20։00 ',
                        'ru' => 'ПОН - ПЯТНИЦА: 08։00֊21։00 <br> СУББ - ВОСК: 10։00֊20։00 ',
                        'en' => 'MON - FRIDAY: 08։00֊21։00 <br> SAT - SUN: 10։00֊20։00 ',
                    ],
                    'phone' => '374 44 71 07 04',
                    'email' => 'sushiart.am@mail.ru',
                    'facebook' => 'https://www.facebook.com/SushiArt.am/',
                    'instagram' => 'https://www.instagram.com/sushiart.am/',
                ],

            ],
        ];

        Page::query()->delete();
        foreach ($pages as $slug => $page){
            Page::query()->create([
                'data' => json_encode($page),
                'slug' => $slug,
            ]);
        }



    }
}
