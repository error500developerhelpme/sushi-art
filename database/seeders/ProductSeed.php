<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryTranslate;
use App\Models\Country;
use App\Models\CountryTranslate;
use App\Models\Product;
use App\Models\ProductTranslate;
use App\Models\Slider;
use App\Models\SliderTranslate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $array = [
            [
                'category_id' => 1,
                'price' => '5000',
                'sale' => '2000',
                'percent' => '19',
                'translate' => [
                    [
                        'title' => 'Սեթեր',
                        'description' => 'Սեթեր',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Сеты',
                        'description' => 'Сеты',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Sets',
                        'description' => 'Sets',
                        'lang' => 'en',
                    ],
                ],
                'images' => [
                    'assets/custom/img/sushi/7.jpg',
                    'assets/custom/img/sushi/8.jpg',
                    'assets/custom/img/sushi/9.jpg',
                ]
            ],
            [
                'category_id' => 1,
                'price' => '8000',
                'sale' => null,
                'translate' => [
                    [
                        'title' => 'Բում առաջարկ',
                        'description' => '<h2 style="margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); font-size: 24px; padding: 0px;">What is Lorem Ipsum?</h2><p style="margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); font-size: 24px; padding: 0px;"><br></p><p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; color: rgb(0, 0, 0); padding: 0px; text-align: justify;"><span style="font-weight: bolder; margin: 0px; padding: 0px;">Lorem Ipsum</span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Гарячий предложение',
                        'description' => 'Гарячий предложение',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Hot offer',
                        'description' => 'Hot offer',
                        'lang' => 'en',
                    ],
                ],
                'images' => [
                    'assets/custom/img/sushi/4.jpg',
                    'assets/custom/img/sushi/5.jpg',
                    'assets/custom/img/sushi/6.jpg',
                ]
            ],
            [
                'category_id' => 2,
                'price' => '15000',
                'sale' => '8000',
                'percent' => '40',
                'translate' => [
                    [
                        'title' => 'Նորույթ',
                        'description' => 'Նորույթ',
                        'lang' => 'am',
                    ],
                    [
                        'title' => 'Новинка',
                        'description' => 'Новинка',
                        'lang' => 'ru',
                    ],
                    [
                        'title' => 'Latest',
                        'description' => 'Latest',
                        'lang' => 'en',
                    ],
                ],
                'images' => [
                    'assets/custom/img/sushi/1.jpg',
                    'assets/custom/img/sushi/2.jpg',
                    'assets/custom/img/sushi/3.jpg',
                ]
            ],

        ];


        foreach ($array as $val){
            $item = Product::query()->create([
                'category_id' => $val['category_id'],
                'price' => $val['price'],
                'sale' => $val['sale'],
                'percent' => $val['percent'] ?? null,
            ]);


            foreach ($val['images'] as $image){
                $item->images()->create(
                    [
                        'path' => $image,
                    ]
                );
            }

            foreach ($val['translate'] as $translate){
                    ProductTranslate::query()->create([
                        'product_id' => $item->id,
                        'lang' => $translate['lang'],
                        'title' => $translate['title'],
                        'description' => $translate['description'],
                    ]);
            }
        }



    }
}
