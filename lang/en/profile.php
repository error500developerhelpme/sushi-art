<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logout' => 'Logout',
    'about_me' => 'About Me',
    'location' => 'Location',
    'number' => 'Phone number',
    'history' => 'History',
    'order' => 'Order',
    'settings' => 'Settings',
    'order_again' => 'Order again',
    'pending' => 'Pending',
    'in_process' => 'In process',
    'done' => 'Done',
    'sent' => 'Sent',
    'finished' => 'Finished',
];
