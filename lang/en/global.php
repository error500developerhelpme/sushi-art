<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'amd' => 'AMD',
    'account_blocked' => 'Account blocked',
    'footer_subscribe' => 'Subscribe',
    'footer_subscribe_text' => 'Subscribe to our mailing list to get the latest updates.',
    'footer_text' => 'We are on the market',
    'footer_title_link' => 'Get in Touch',
    'footer_sub_title_link' => 'Online Shop',
    'footer_page' => 'Pages',
    'about_us' => 'About us',
    'sale' => 'Sale! ',
    'upto' => ' Upto',
    'off' => 'off',
    'email' => 'Email',
    'name' => 'Name',
    'password' => 'Password',
    'save' => 'Save',
    'all' => 'All',
    'search' => 'Search',
    'quantity' => 'Quantity',
    'price' => 'Price',
    'phone' => 'Phone',
    'city' => 'City',
    'address' => 'Address',
    'product_image' => 'Product Image',
    'total' => 'Total',
    'sub_total' => 'Subtotal',
    'shipping' => 'Shipping',
    'check_out' => 'Check Out',
    'shipping_address' => 'Shipping Address',
    'card' => 'Card',
    'cash' => 'Cash',
    'place_order' => 'Place Order',
    'shop_address' => 'Shop Address',
    'shop_hours' => 'Shop Hours',
    'contact' => 'Contact',
    'our_location' => 'Find Our Location',
    'order_again' => 'Order again',
    'category' => 'Category',
    'related' => 'Related',
    'products' => 'Products',
    'product' => 'Product',
    'fresh_and_delicious' => 'Fresh and delicious',
    'success' => 'Success',
];
