<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'shop_now' => 'Shop new',
    'add_to_card' => 'Add to Cart',
    'more' => 'More',
    'see_product' => 'See product',
    'submit' => 'Submit',
    'close' => 'Close',
    'save_and_submit' => 'Save and submit',

];
