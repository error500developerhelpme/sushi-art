<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'profile_header_subtitle' => 'Settings',
    'profile_header_title' => 'Profile',

    'shop_header_subtitle' => 'Fresh and Organic',
    'shop_header_title' => 'Shop',

    'cart_header_subtitle' => 'Fresh and Organic',
    'cart_header_title' => 'Cart',

    'check_out_header_subtitle' => 'Fresh and Organic',
    'check_out_header_title' => 'Order',

    'contact_header_subtitle' => 'Get 24/7 Support',
    'contact_header_title' => 'Contact us',

    'single_product_header_subtitle' => 'See more Details',
    'single_product_header_title' => 'Single Product',
];
