<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'email' => 'Email',
    'name' => 'Name',
    'password' => 'Password',
    'old_password' => 'Old Password',
    'password_confirmation' => 'Password confirmation',
    'phone' => 'Phone',
    'city' => 'City',
    'address' => 'Address',
    'search' => 'Search',
    'location_url' => 'Location URL',
    'message' => 'Message',
    'title' => 'Title',
];
