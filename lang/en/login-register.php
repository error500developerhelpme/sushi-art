<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //login
    'login' => 'Login',
    'text_register_login' => "Don't have an account yet?",
    'go_to_register' => 'Go to Register',

    //register
    'register' => 'Register',
    'text_login_register' => "Already have an account?",
    'go_to_login' => 'Go to Login',
];
