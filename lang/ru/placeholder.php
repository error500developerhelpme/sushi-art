<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'email' => 'Эл. почта',
    'name' => 'Имя',
    'password' => 'Пароль',
    'old_password' => 'Старый пароль',
    'password_confirmation' => 'Подтверждение пароля',
    'phone' => 'Телефон',
    'city' => 'Город',
    'address' => 'Адрес',
    'search' => 'Поиск',
    'location_url' => 'Расположение URL-адрес',
    'message' => 'Сообщение',
    'title' => 'Заголовок',
];
