<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'amd' => 'драм',
    'account_blocked' => 'Аккаунт заблокирован',
    'footer_subscribe' => 'Подписка',
    'footer_subscribe_text' => 'Подпишитесь на нашу рассылку, чтобы получать последние обновления.',
    'footer_text' => 'Мы на рынке',
    'footer_title_link' => 'Связаться',
    'footer_sub_title_link' => 'Интернет магазин',
    'footer_page' => 'Страницы',
    'about_us' => 'О нас',
    'sale' => 'Распродажа! ',
    'upto' => 'до',
    'off' => 'Скидка',
    'email' => 'Эл. почта',
    'name' => 'Имя',
    'password' => 'Пароль',
    'save' => 'Сохранять',
    'all' => 'Все',
    'search' => 'Поиск',
    'quantity' => 'Количество',
    'price' => 'Цена',
    'phone' => 'Телефон',
    'city' => 'Город',
    'address' => 'Адрес',
    'product_image' => 'Изображение продукта',
    'total' => 'Общий',
    'sub_total' => 'Промежуточный итог',
    'shipping' => 'Перевозка',
    'check_out' => 'Проверить',
    'shipping_address' => 'Адрес доставки',
    'order_details' => 'Информация для заказа',
    'card' => 'Информация для заказа',
    'cash' => 'Наличные',
    'place_order' => 'Разместить заказ',
    'shop_address' => 'Адрес магазина',
    'shop_hours' => 'Часы работы магазина',
    'contact' => 'Контакт',
    'our_location' => 'Найдите наше местоположение',
    'order_again' => 'Заказать еще раз',
    'category' => 'Категория',
    'related' => 'Связанный',
    'products' => 'Продукты',
    'product' => 'Продукт',
    'fresh_and_delicious' => 'Свежий и вкусный',
    'success' => 'Подтвержденный',

];
