<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'our' => 'Наши',
    'products' => 'продукты',
    'description_our_products' => 'Полезно и вкусно, аппетитно и свежо',
];
