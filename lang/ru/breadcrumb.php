<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'profile_header_subtitle' => 'Настройки',
    'profile_header_title' => 'Профиль',

    'shop_header_subtitle' => 'Свежий и органический',
    'shop_header_title' => 'Магазин',

    'cart_header_subtitle' => 'Свежий и органический',
    'cart_header_title' => 'Корзина',

    'check_out_header_subtitle' => 'Свежий и органический',
    'check_out_header_title' => 'Заказ',

    'contact_header_subtitle' => '24/7 поддержка',
    'contact_header_title' => 'Связаться с нами',

    'single_product_header_subtitle' => 'Одиночный продукт',
    'single_product_header_title' => 'Посмотреть подробнее',
];
