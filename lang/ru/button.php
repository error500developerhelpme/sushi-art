<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'shop_now' => 'Магазин',
    'add_to_card' => 'Добавить в корзину',
    'more' => 'Более',
    'see_product' => 'Посмотреть продукт',
    'submit' => 'Отправлять',
    'close' => 'Закрывать',
    'save_and_submit' => 'Сохранить и отправить',
];
