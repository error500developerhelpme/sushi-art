<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Авторизоваться',
    'text_register_login' => "У вас еще нет учетной записи?",
    'go_to_register' => 'Перейти к регистрации',

    //register
    'register' => 'Регистрация',
    'text_login_register' => "У вас уже есть аккаунт?",
    'go_to_login' => 'Перейти к авторизации',
];
