<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logout' => 'Выйти',
    'about_me' => 'Обо мне',
    'location' => 'Расположение',
    'number' => 'Номер телефона',
    'history' => 'История',
    'order' => 'Заказ',
    'settings' => 'Настройки',
    'order_again' => 'Заказать еще раз',
    'status' => 'Положение дел',
    'pending' => 'В ожидании',
    'in_process' => 'В процессе',
    'done' => 'Сделанный',
    'sent' => 'Отправил',
    'finished' => 'Законченный',

];
