<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'profile_header_subtitle' => 'Կարգավորումներ',
    'profile_header_title' => 'Անձնական էջ',

    'shop_header_subtitle' => 'Թարմ և օրգանական',
    'shop_header_title' => 'Խանութ',

    'cart_header_subtitle' => 'Թարմ և օրգանական',
    'cart_header_title' => 'Սայլակ',

    'check_out_header_subtitle' => 'Թարմ և օրգանական',
    'check_out_header_title' => 'Պատվեր',

    'contact_header_subtitle' => '24/7 աջակցություն',
    'contact_header_title' => 'Կապվեք մեզ հետ',

    'single_product_header_subtitle' => 'Մեկ արտադրանք',
    'single_product_header_title' => 'Տես ավելի մանրամասն',
];
