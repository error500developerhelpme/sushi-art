<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Մուտք',
    'text_register_login' => "Դեռ չունե՞ք էջ մեր կայքում:",
    'go_to_register' => 'Գրանցում',


    //register
    'register' => 'Գրանցվել',
    'text_login_register' => "Արդեն ունե՞ք էջ մեր կայքում?",
    'go_to_login' => 'Մուտք',
];
