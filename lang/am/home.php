<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'our' => 'Մեր',
    'products' => 'արտադրանքը',
    'description_our_products' => 'Օգտակար և համեղ, ախորժելի և թարմ',
];
