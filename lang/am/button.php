<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'shop_now' => 'Տեսնել ավելին',
    'add_to_card' => 'Ավելացնել զամբյուղ',
    'more' => 'Ավելին',
    'see_product' => 'Տեսնել ապրանքը',
    'submit' => 'Ուղարկել',
    'close' => 'Փակել',
    'save_and_submit' => 'Պահպանել և ներկայացնել',

];
