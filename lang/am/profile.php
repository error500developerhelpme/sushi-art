<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header_subtitle' => 'Կարգավորումներ',
    'header_title' => 'Անձնագիր',
    'logout' => 'Դուրս գալ',
    'about_me' => 'Իմ մասին',
    'location' => 'Գտնվելու վայրը',
    'number' => 'Հեռախոսահամար',
    'history' => 'Պատմություն',
    'order' => 'Պատվեր',
    'settings' => 'Կարգավորումներ',
    'order_again' => 'Կրկին պատվիրեք',
    'status' => 'Կարգավիճակ',
    'pending' => 'Սպասվում է',
    'in_process' => 'Ընթացքում',
    'done' => 'Կատարած',
    'sent' => 'Ուղարկված',
    'finished' => 'Ավարտված',
];
