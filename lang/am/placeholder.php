<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'email' => 'Էլ. հասցե',
    'name' => 'Անուն',
    'password' => 'Գաղտնաբառ',
    'old_password' => 'Հին Գաղտնաբառ',
    'password_confirmation' => 'Գաղտնաբառի հաստատում',
    'phone' => 'Հեռախոս',
    'city' => 'Քաղաք',
    'address' => 'Հասցե',
    'search' => 'Որոնում',
    'location_url' => 'Գտնվելու վայրը URL',
    'message' => 'Հաղորդագրություն',
    'title' => 'Վերնագիր',
];
