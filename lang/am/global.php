<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'amd' => 'դրամ',
    'account_blocked' => 'Էջը բլոկաորված է',
    'footer_subscribe' => 'Բաժանորդագրում',
    'footer_subscribe_text' => 'Բաժանորդագրվեք մեր տեղեկագրին՝ վերջին թարմացումները ստանալու համար:',
    'footer_text' => 'Մենք շուկայում ենք',
    'footer_title_link' => 'Կապվեք',
    'footer_sub_title_link' => 'Օնլայն խանութ',
    'footer_page' => 'Էջեր',
    'about_us' => 'Մեր մասին',
    'sale' => 'Վաճառք ',
    'upto' => ' Մինչև',
    'off' => 'զեղչ',
    'email' => 'Էլ. հասցե',
    'name' => 'Անուն',
    'password' => 'Գաղտնաբառ',
    'save' => 'Պահպանել',
    'all' => 'Բոլորը',
    'search' => 'Որոնում',
    'quantity' => 'Քանակ',
    'price' => 'Գին',
    'phone' => 'Հեռախոս',
    'city' => 'Քաղաք',
    'address' => 'Հասցե',
    'product_image' => 'Ապրանքի պատկեր',
    'total' => 'Ընդամենը',
    'sub_total' => 'Ենթագումար',
    'shipping' => 'Առաքում',
    'check_out' => 'Ստուգել',
    'shipping_address' => 'Աոաքման Հասցե',
    'order_details' => 'Պատվերի մանրամասները',
    'card' => 'Քարտ',
    'cash' => 'Կանխիկ',
    'place_order' => 'Պատվիրել',
    'shop_address' => 'Խանութի հասցեն',
    'shop_hours' => 'Աշխատանքային ժամեր',
    'contact' => 'Կապ',
    'our_location' => 'Մեր գտնվելու վայրը',
    'order_again' => 'Կրկին պատվիրեք',
    'category' => 'Կատեգորիա',
    'related' => 'Առնչվող',
    'products' => 'Ապրանքներ',
    'product' => 'Ապրանք',
    'fresh_and_delicious' => 'Թարմ և համեղ',
    'success' => 'Հաստատված է',
];
