<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sushi Art">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- title -->
    <title>Sushi Art</title>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="{{asset('assets/custom/img/ART_V2.png')}}">

    @include('auth.components.css')

    @yield('css')

</head>
<body>

<!--PreLoader-->
@include('frontend.components.loader')
<!--PreLoader Ends-->

{{--@include('frontend.components.navbar')--}}


@yield('content')



<!-- copyright -->
{{--@include('frontend.components.footer')--}}
<!-- end copyright -->

@include('auth.components.js')

@yield('js')

</body>
</html>
