{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Login') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-0">--}}
{{--                            <div class="col-md-8 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Login') }}--}}
{{--                                </button>--}}

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}



@extends('layouts.authorization')

@section('css')
    <style>
        .is-invalid{
            border: 1px solid #ff00008f;
        }

        @media only screen and (max-width: 600px) {
            .form-row {
                width: 80%;
                margin: 0 auto;
                margin-bottom: 15px;
            }
        }
        .form-row .form-control:first-child {
             margin-right: 0px;
        }
    </style>
@endsection

@section('content')

    <div class="wrapper" style="background: url({{asset('assets/custom/img/fon.jpg')}}) no-repeat center center;">
        <div class="inner">
            <div class="image-holder">
                <a href="{{route('home.page')}}"><img src="{{asset('assets/custom/img/ART_V2.png')}}" alt=""></a>
                {{--                <img src="{{asset('assets/custom/img/sushi/36.png')}}" alt="">--}}
            </div>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <h3>{{__('login-register.login')}}</h3>
                <div class="form-row" style="justify-content: space-around;">
                    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="{{__('placeholder.email')}}" value="{{old('email')}}">
                </div>
                <div class="form-row" style="justify-content: space-around;">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"  placeholder="{{__('placeholder.password')}}">
                </div>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <small>{{ $error }}</small>
                        </span>
                    @endforeach
                @endif
                <button>
                    {{__('login-register.login')}}
                    <i class="zmdi zmdi-long-arrow-right"></i>
                </button>
                <div style="display: block; text-align: center; margin-top: 10px">
                    <small>{{__('login-register.text_register_login')}}  <a href="{{route('register')}}" style="color: #F28123">{{__('login-register.go_to_register')}}</a></small>
                </div>
            </form>

        </div>
    </div>


@endsection
