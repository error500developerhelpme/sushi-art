{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Register') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                                @error('name')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

{{----------------------------------------------------------------------------------------------------------}}

@extends('layouts.authorization')

@section('css')
    <style>
        .is-invalid{
            border: 1px solid #ff00008f;
        }
        @media only screen and (max-width: 600px) {
            .form-row {
                width: 80%;
                margin: 0 auto;
                margin-bottom: 15px;
            }
        }
    </style>
@endsection

@section('content')

    <div class="wrapper" style="background: url({{asset('assets/custom/img/fon.jpg')}}) no-repeat center center;">
        <div class="inner">
            <div class="image-holder">
                <a href="{{route('home.page')}}"><img src="{{asset('assets/custom/img/ART_V2.png')}}" alt=""></a>
{{--                <img src="{{asset('assets/custom/img/sushi/36.png')}}" alt="">--}}
            </div>
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <h3>{{__('login-register.register')}}</h3>
                <div class="form-row">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="{{__('placeholder.name')}}" value="{{old('name')}}">
                    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="{{__('placeholder.email')}}" value="{{old('email')}}">
                </div>
                <div class="form-row">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"  placeholder="{{__('placeholder.password')}}">
                    <input type="password" class="form-control @error('name') is-invalid @enderror" name="password_confirmation" placeholder="{{__('placeholder.password_confirmation')}}">
                </div>
                <div class="form-row">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="phone" placeholder="{{__('placeholder.phone')}}" value="{{old('phone')}}">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="city" placeholder="{{__('placeholder.city')}}" value="{{old('city')}}">
                </div>
                <div class="form-row">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="address" placeholder="{{__('placeholder.address')}}" value="{{old('address')}}">
                </div>

                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <small>{{ $error }}</small>
                        </span>
                    @endforeach
                @endif

                <button>
                    {{__('login-register.register')}}
                    <i class="zmdi zmdi-long-arrow-right"></i>
                </button>
                <div style="display: block; text-align: center; margin-top: 10px">
                    <small>{{__('login-register.text_login_register')}}  <a href="{{route('login')}}" style="color: #F28123">{{__('login-register.go_to_login')}}</a></small>
                </div>
            </form>

        </div>
    </div>


@endsection
