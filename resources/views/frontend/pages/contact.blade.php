@extends('layouts.app')



@section('css')

@endsection()

@section('content')


    <!-- breadcrumb-section -->
    @include('frontend.components.breadcrumb', ['subtitle'=>__('breadcrumb.contact_header_subtitle'), 'title'=>__('breadcrumb.contact_header_title'), 'path'=>'assets/custom/img/IMG_2293.jpg'])
    <!-- end breadcrumb section -->

    <!-- contact form -->
    <div class="contact-from-section mt-150 mb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="form-title">
                        <h2>Ձեր կարծիքը/բողոքը/առաջարկը</h2>
                        <p>Ձեր կողմից գրված կարծիքը անանուն է կարող եք գրել Ձեր կարքիը առանց քաշվելու</p>
                        <p>Դուք կարող եք գրել Ձեր կարծիքը կամ բողոքը կամ առաջարկը կայքի և ոչ միայն, նաև առաքման կամ ուտելիքի որակի հետ կապված, տվյալ նամակը կարդում է անձամփ տնորենը, Ձեր կարծիքը շատ կարևոր է մեզ և մեր կայքի բարելավման համար, Շնորհակալություն սիրով, SushiArt թիմ։</p>
                    </div>
                    <div id="form_status"></div>
                    <div class="contact-form">
                        <form method="POST" action="{{route('contact.application.proposal')}}" id="">
                            @method('POST')
                            @csrf
                            <p><input type="text" placeholder="{{__('placeholder.title')}}" name="title" id="title"></p>
                            <p><textarea name="message" id="message" cols="30" rows="10" placeholder="{{__('placeholder.message')}}"></textarea></p>
                            <p><input type="submit" value="{{__('button.submit')}}"></p>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="contact-form-wrap">
                        <div class="contact-form-box">
                            <h4><i class="fas fa-map"></i> {{__('global.shop_address')}}</h4>
                            <p>{{get_page_model_translate_value($pageGLOBAL['contact_info']['shop_address'])}}</p>
                        </div>
                        <div class="contact-form-box">
                            <h4><i class="far fa-clock"></i> {{__('global.shop_hours')}}</h4>
                            <p>{!! get_page_model_translate_value($pageGLOBAL['contact_info']['shop_hours']) !!}</p>
                        </div>
                        <div class="contact-form-box">
                            <h4><i class="fas fa-address-book"></i> {{__('global.contact')}}</h4>
                            <p>{{__('global.phone')}}:
                                <a href="tel:{{$pageGLOBAL['contact_info']['phone']}}" class="global-color">+{{$pageGLOBAL['contact_info']['phone']}}</a>
                                <br>
                                {{__('global.email')}}:
                                <a href="mailto:{{$pageGLOBAL['contact_info']['email']}}" class="global-color">{{$pageGLOBAL['contact_info']['email']}}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end contact form -->

    <!-- find our location -->
    <div class="find-location blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p> <i class="fas fa-map-marker-alt"></i> {{__('global.our_location')}}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end find our location -->

    <!-- google map section -->
    <div class="embed-responsive embed-responsive-21by9">
        <div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Yerevan+(Yerevan)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"><a href="https://www.maps.ie/population/">Calculate population in area</a></iframe></div>
{{--        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26432.42324808999!2d-118.34398767954286!3d34.09378509738966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf07045279bf%3A0xf67a9a6797bdfae4!2sHollywood%2C%20Los%20Angeles%2C%20CA%2C%20USA!5e0!3m2!1sen!2sbd!4v1576846473265!5m2!1sen!2sbd" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" class="embed-responsive-item"></iframe>--}}
    </div>
    <!-- end google map section -->




@endsection()
