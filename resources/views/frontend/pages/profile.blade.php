@extends('layouts.app')



@section('css')


    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/backend/dist/css/adminlte.min.css')}}">

    <style>
        .card-primary.card-outline{
            border-top: 3px solid #e48123;
        }
        .card-primary:not(.card-outline)>.card-header {
            background-color: #e48123;
        }
        .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
            color: #fff;
            background-color: #e48123;
        }
        .btn-primary {
            color: #fff;
            background-color: #e48123;
            border-color: #e48123;
            box-shadow: none;
        }
        .btn-primary:hover {
            color: #fff;
            background-color: #ab6018;
            border-color: #ab6018;
        }
        input[type="number"] {
            border: 1px solid #ddd;
            border-radius: 5px;
            /* padding: 10px; */
            width: 100%;
             margin-bottom: 0px;
        }
        .class-row-class{
            display: flex;
            justify-content: space-around;
            flex-direction: row;
            flex-wrap: nowrap;
        }

        .image-order-profile{
            height: 200px;
            background-repeat: no-repeat;
            background-position: center;
            margin: 5px;
            background-size: 100%;
            padding: 0;
        }

        .image-order-profile:hover{

        }
        .status-order-profile{
            background-color: #e48123;
            padding: 5px;
            border:1px solid #ff7c00;
            border-radius: 10px;
            color: white;
            text-transform: capitalize
        }

        .order-product-info{
            width: 100%;
            height: 200px;
            background-color: #0a0a0ebd;
            padding: 20px;
            text-align: center;
        }
        .order-product-info p{
            color: white;
        }

        .order-product-single-profile{
            color: #eef100;
        }

        .order-product-single-profile:hover{
            color: #939501;
        }


        .validate {
            border: 1px solid red !important;
        }
    </style>

@endsection()

@section('content')
{{--{{dd($title, $subtitle, __('profile.header_subtitle'))}}--}}
    <!-- breadcrumb-section -->
    @include('frontend.components.breadcrumb', ['subtitle'=>__('breadcrumb.profile_header_subtitle'), 'title'=>__('breadcrumb.profile_header_title'), 'path'=>'assets/custom/img/IMG_2293.jpg'])
    <!-- end breadcrumb section -->

    <!-- Main content -->
    <section class="content" style="margin: 50px ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
{{--                            <div class="text-center">--}}
{{--                                <img class="profile-user-img img-fluid img-circle"--}}
{{--                                     src="../../dist/img/user4-128x128.jpg"--}}
{{--                                     alt="User profile picture">--}}
{{--                            </div>--}}

                            <h3 class="profile-username text-center">{{auth()->user()->name}}</h3>

{{--                            <p class="text-muted text-center">Type: {{auth()->user()->type}}</p>--}}
                            <p class="text-muted text-center"><br></p>

{{--                            <ul class="list-group list-group-unbordered mb-3">--}}
{{--                                <li class="list-group-item">--}}
{{--                                    <b>All orders</b> <a class="float-right">{{$ordersCount}}</a>--}}
{{--                                </li>--}}
{{--                                <li class="list-group-item">--}}
{{--                                    <b>Pending</b> <a class="float-right">543</a>--}}
{{--                                </li>--}}
{{--                                <li class="list-group-item">--}}
{{--                                    <b>Done</b> <a class="float-right">13,287</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}

                            <a href="#" class="btn btn-primary btn-block"
                               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <b>{{__('profile.logout')}}</b>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{__('profile.about_me')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <strong><i class="fas fa-book mr-1"></i>{{__('global.email')}}</strong>

                            <p class="text-muted">
                                {{auth()->user()->email}}
                            </p>

                            <hr>

                            <strong><i class="fas fa-map-marker-alt mr-1"></i> {{__('profile.location')}}</strong>

                            <p class="text-muted">{{auth()->user()->information->city ?? ''}} {{auth()->user()->information->address ?? ''}}</p>

                            <hr>

                            <strong><i class="fas fa-pencil-alt mr-1"></i> {{__('profile.number')}}</strong>

                            <p class="text-muted">
                                {{auth()->user()->information->phone ?? ''}}
                            </p>

                            <hr>

{{--                            <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>--}}

{{--                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>--}}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#history" data-toggle="tab">{{__('profile.history')}}</a></li>
                                <li class="nav-item"><a class="nav-link " href="#order" data-toggle="tab">{{__('profile.order')}}</a></li>
                                <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">{{__('profile.settings')}}</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="history">
                                    <!-- The timeline -->
                                    @foreach($orderHistory as $order)
                                        @if(in_array($order->status, ['finished', 'canceled']))
                                            <div class="timeline timeline-inverse">
                                                <!-- timeline time label -->
                                            <!-- END timeline item -->
                                                <!-- timeline time label -->
                                                <div class="time-label">
                                                    <span class="bg-danger">
                                                      {{\Carbon\Carbon::make($order->created_at)->format('d-m-Y h:m:s A')}}
                                                    </span>
                                                </div>
                                                <!-- /.timeline-label -->
                                                <!-- timeline item -->
                                                <div>
                                                    @if($order->status == 'finished')
                                                        <i class="far  fa-flag bg-green"></i>
                                                    @else
                                                        <i class="far fa-clock bg-gray"></i>
                                                    @endif
                                                    <div class="timeline-item">
                                                        <span class="time"><i class="far fa-clock"></i>  {{\Carbon\Carbon::make($order->updated_at)->format('d-m-Y h:m:s A')}}</span>

                                                        <h3 class="timeline-header">
                                                            <span class="status-order-profile">{{ __('profile.'.$order->status)  }}</span>
                                                        </h3>

                                                        <!-- Button trigger modal -->
                                                        <span class="time col-12" style="padding: 2px">
                                                            <button type="button" class="btn btn-primary col-12" data-toggle="modal" data-target="#orderAgain">
                                                                {{__('profile.order_again')}}
                                                            </button>
                                                        </span>
                                                        <!-- modal -->
                                                        @include('frontend.components.modal', ['type' => 'orderAgain', 'order' => $order])
{{--                                                        <span class="time"><i class="far fa-clock"></i> Order <a href="{{route('order.again', $order->id)}}" onclick="return confirm('Are you sure ?');">again</a></span>--}}
                                                        <h3 class="timeline-header">

                                                            <span>{{__('global.price')}}: @if(isset($order->sale))<i class="fa fa-gift" aria-hidden="true"></i> {{$order->sale}} @else {{$order->price}} @endif {{__('global.amd')}}</span>
                                                        </h3>

                                                        <div class="timeline-body row">
                                                            @foreach($order->card as $card)
                                                                <div class="main-image-order-profile image-order-profile col-xl-3 col-sm-12 col-md-12 "  style="background-size: cover;background-image: url({{asset(has_file($card->product->images[0]->path ?? '', 'product'))}}); ">
                                                                    <div class="order-product-info">
                                                                        <p>{{__('global.quantity')}}: {{$card->quantity}}</p>
                                                                        <p>{{__('global.name')}}: <a class="order-product-single-profile" href="{{route('single.product', $card->product->id)}}">{{\App\Hellper\GetLocalizedValue::GetValue($card->product, 'title')}}</a></p>
                                                                    </div>
                                                                </div>
{{--                                                            <img src="{{asset($card->product->images[0]->path)}}" alt="..." class="image-order-profile" style="width: 20%;">--}}
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END timeline item -->
                                                <div>
                                                    <i class="fa  fa-flag bg-green"></i>
                                                </div>

                                                <br>
                                                <div class="time-label">
                                                    <span class="bg-success">
                                                      {{\Carbon\Carbon::make($order->updated_at)->format('d-m-Y h:m:s A')}}
                                                    </span>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif
                                    @endforeach
                                </div>
                                <!-- /.tab-pane -->
                                <div class=" tab-pane" id="order">
                                    <!-- The timeline -->
                                    @foreach($orderWaiting as $order)
                                        @if(!in_array($order->status, ['finished', 'canceled']))
                                            <div class="timeline timeline-inverse">
                                                <!-- timeline time label -->
                                                <div class="time-label">
                                                    <span class="bg-danger">
                                                      {{\Carbon\Carbon::make($order->created_at)->format('d-m-Y h:m:s A')}}
                                                    </span>
                                                </div>
                                                <!-- /.timeline-label -->
                                                <!-- timeline item -->
                                                <div>
                                                    @if($order->status == 'pending')
                                                        <i class="fas fa-shopping-bag bg-purple"></i>
                                                    @elseif($order->status == 'in_process')
                                                        <i class="fa fa-fire bg-red" aria-hidden="true"></i>
                                                    @elseif($order->status == 'done')
                                                        <i class="fa fa-check bg-green"></i>
                                                    @elseif($order->status == 'sent')
                                                        <i class="fa fa-taxi bg-yellow"></i>
                                                    @elseif($order->status == 'finished')
                                                        <i class="far  fa-flag bg-green"></i>
                                                    @else
                                                        <i class="far fa-clock bg-gray"></i>
                                                    @endif

                                                    <div class="timeline-item">
                                                        <span class="time"><i class="far fa-clock"></i>  {{\Carbon\Carbon::make($order->updated_at)->format('d-m-Y h:m:s A')}}</span>

                                                        <h3 class="timeline-header">
                                                            <a href="javascript:void(0)">{{__('profile.status')}}</a>: <span class="status-order-profile">{{ __('profile.'.$order->status)  }}</span>
                                                        </h3>
                                                        <h3 class="timeline-header"><span>{{__('global.price')}}: @if(isset($order->sale))<i class="fa fa-gift" aria-hidden="true"></i> {{$order->sale}} @else {{$order->price}} @endif {{__('global.amd')}}</span></h3>

                                                        <div class="timeline-body row">
                                                            @foreach($order->card as $card)
                                                                <div class="main-image-order-profile image-order-profile col-xl-3 col-sm-12 col-md-12" style="background-size: cover;background-image: url({{asset(has_file($card->product->images[0]->path, 'product'))}}); ">
                                                                    <div class="order-product-info">
                                                                        <p>{{__('profile.quantity')}}: {{$card->quantity}}</p>
                                                                        <p>{{__('global.name')}}: <a class="order-product-single-profile" href="{{route('single.product', $card->product->id)}}">{{\App\Hellper\GetLocalizedValue::GetValue($card->product, 'title')}}</a></p>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END timeline item -->
                                                @if($order->status == 'finished')
                                                <div>
                                                    <i class="fa  fa-flag bg-green"></i>
                                                </div>

                                                    <br>
                                                    <div class="time-label">
                                                        <span class="bg-success">
                                                          {{\Carbon\Carbon::make($order->updated_at)->format('d-m-Y h:m:s A')}}
                                                        </span>
                                                    </div>
                                                @else
                                                    <div>
                                                        <i class="far fa-clock bg-gray"></i>
                                                    </div>
                                                @endif
                                            </div>
                                            <hr>
                                        @endif
                                    @endforeach
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="settings">
                                    <form   method="POST" action="{{ route('settings.user') }}" class="form-horizontal">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 col-form-label">{{__('global.name')}}</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control  {{isset($errors->default->toArray()['name']) ? 'validate' : ''}}"
                                                       id="inputName" name="name" placeholder="{{__('placeholder.name')}}" value="{{auth()->user()->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail" class="col-sm-2 col-form-label">{{__('global.email')}}</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control {{isset($errors->default->toArray()['email']) ? 'validate' : ''}}"
                                                       id="inputEmail" placeholder="{{__('placeholder.email')}}" disabled  value="{{auth()->user()->email}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">{{__('global.password')}}</label>
                                            <div class="col-sm-10 row  class-row-class">
                                                <input type="password" class="form-control col-6" id="inputPassword" name="password" placeholder="{{__('placeholder.password')}}" style="margin-right: 3px; margin-left: 15px;">
                                                <input type="password" class="form-control col-6" id="inputOldPassword" name="old_password" placeholder="{{__('placeholder.old_password')}}">
                                            </div>
                                        </div>

{{--                                        <div class="form-group row">--}}
{{--                                            <label for="inputExperience" class="col-sm-2 col-form-label">Notes</label>--}}
{{--                                            <div class="col-sm-10">--}}
{{--                                                <textarea class="form-control" id="inputExperience" placeholder="Notes"></textarea>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="form-group row">
                                            <label for="inputPhone" class="col-sm-2 col-form-label">{{__('global.phone')}}</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control {{isset($errors->default->toArray()['phone']) ? 'validate' : ''}}"
                                                       id="inputPhone" name="phone" placeholder="{{__('placeholder.phone')}}" value="{{auth()->user()->information->phone ?? ''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputCity" class="col-sm-2 col-form-label">{{__('global.city')}}</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control {{isset($errors->default->toArray()['city']) ? 'validate' : ''}}"
                                                       id="inputCity" name="city" placeholder="{{__('placeholder.city')}}" value="{{auth()->user()->information->city ?? ''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputAddress" class="col-sm-2 col-form-label">{{__('global.address')}}</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control {{isset($errors->default->toArray()['address']) ? 'validate' : ''}}"
                                                       id="inputAddress" name="address" placeholder="{{__('placeholder.address')}}" value="{{auth()->user()->information->address ?? ''}}">
                                            </div>
                                        </div>
{{--                                        <div class="form-group row">--}}
{{--                                            <div class="offset-sm-2 col-sm-10">--}}
{{--                                                <div class="checkbox">--}}
{{--                                                    <label>--}}
{{--                                                        <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="form-group row">
                                            <div class="offset-sm-2 col-sm-10">
                                                <button type="submit" class="btn btn-danger">{{__('global.save')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->



@endsection()

