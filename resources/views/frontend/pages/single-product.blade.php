@extends('layouts.app')



@section('css')

@endsection()

@section('content')


    <!-- breadcrumb-section -->
    @include('frontend.components.breadcrumb', ['subtitle'=>__('breadcrumb.single_product_header_subtitle'), 'title'=>__('breadcrumb.single_product_header_title'), 'path'=>'assets/custom/img/IMG_2293.jpg'])
    <!-- end breadcrumb section -->

    <!-- single product -->
    <div class="single-product mt-150 mb-150">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="single-product-img">
                        @foreach($product->images as $key => $image)
                            @if($key == 0)
                                <a data-fancybox="gallery" href="{{asset(has_file($image->path, 'product'))}}">
                                    <img src="{{asset(has_file($image->path, 'product'))}}">
                                </a>
                            @else
                                <a  data-fancybox="gallery" href="{{asset(has_file($image->path, 'product'))}}">
                                    <img style="display: none" src="{{asset(has_file($image->path, 'product'))}}">
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="single-product-content">
                        <h3>{{ \App\Hellper\GetLocalizedValue::GetValue($product, 'title')  }}</h3>
                        @if(isset($product->sale))
                            <p class="single-product-pricing"><span><del>{{$product->price}}</del> {{__('global.amd')}}</span> {{$product->sale}} {{__('global.amd')}}</p>
                        @else
                            <p class="single-product-pricing"> {{$product->price}} {{__('global.amd')}}</p>
                        @endif
                        <p>{!!  \App\Hellper\GetLocalizedValue::GetValue($product, 'description') !!}</p>
                        <br>
                        <div class="single-product-form">
{{--                            <form action="index.html">--}}
{{--                                <input type="number" placeholder="0">--}}
{{--                            </form>--}}
                            @include('frontend.components.button-add-to-card', ['product_id' => $product->id])
                            <p><strong>{{__('global.category')}}: </strong>{{\App\Hellper\GetLocalizedValue::GetValue($product->category, 'title')}}</p>
                        </div>
                        @include('frontend.components.sharingbuttons', ['message'=>\App\Hellper\GetLocalizedValue::GetValue($product, 'title')])
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end single product -->

    <!-- more products -->
    <div class="more-products mb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center">
                    <div class="section-title">
                        <h3><span class="orange-text">{{__('global.related')}}</span> {{__('global.products')}}</h3>
                        <p>{{__('global.fresh_and_delicious')}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @include('frontend.components.product-card', ['products' => $categoryProduct->products])
            </div>
        </div>
    </div>
    <!-- end more products -->


@endsection()
