@extends('layouts.app')



@section('css')

<style>
    .validate {
        border: 1px solid red !important;
    }
</style>

@endsection()

@section('content')


    <!-- breadcrumb-section -->
    @include('frontend.components.breadcrumb', ['subtitle'=>__('breadcrumb.check_out_header_subtitle'), 'title'=>__('breadcrumb.check_out_header_title'), 'path'=>'assets/custom/img/IMG_2293.jpg'])
    <!-- end breadcrumb section -->

    <!-- check out section -->
    <div class="checkout-section mt-150 mb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="checkout-accordion-wrap">
                        <div class="accordion" id="accordionExample">
                            <div class="card single-accordion">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            {{__('global.shipping_address')}}
                                        </button>
                                    </h5>
                                </div>
{{--                                {{dd($errors->default->toArray()['city'] ?? '')}}--}}

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="billing-address-form">
                                            @guest()
                                                <form class="checkout-form-submit" method="POST" action="{{route('guest.order')}}">
                                            @else
                                                <form class="checkout-form-submit" method="POST" action="{{route('order')}}">
                                            @endguest
                                                @csrf
                                                <p>
                                                    <input
                                                        class="{{isset($errors->default->toArray()['name']) ? 'validate' : ''}}"
                                                        type="text" placeholder="{{__('placeholder.name')}}*" name="name" value="{{auth()->user()->name ?? ''}}" required>
                                                </p>
                                                <p>
                                                    <input
                                                        class="{{isset($errors->default->toArray()['email']) ? 'validate' : ''}}"
                                                        type="email" placeholder="{{__('placeholder.email')}}*" @auth() disabled @endauth value="{{auth()->user()->email ?? ''}}" required>
                                                </p>
                                                <p>
                                                    <input
                                                        class="{{isset($errors->default->toArray()['city']) ? 'validate' : ''}}"
                                                        type="text" placeholder="{{__('placeholder.city')}}*" name="city" value="{{auth()->user()->information->city ?? ''}}" required>
                                                </p>
                                                <p>
                                                    <input
                                                        class="{{isset($errors->default->toArray()['address']) ? 'validate' : ''}}"
                                                        type="text" placeholder="{{__('placeholder.address')}}*" name="address" value="{{auth()->user()->information->address ?? ''}}" required>
                                                </p>
                                                <p>
                                                    <input
                                                        class="{{isset($errors->default->toArray()['phone']) ? 'validate' : ''}}"
                                                        type="tel" placeholder="{{__('placeholder.phone')}}*" name="phone" value="{{auth()->user()->information->phone ?? ''}}" required>
                                                </p>
                                                <p>
                                                    <span class="toggle">
                                                        <input type="radio" name="pay_method" value="card" id="sizeWeight" checked="checked" />
                                                        <label for="sizeWeight" style=" outline: none;">{{__('global.card')}}</label>
                                                        <input type="radio" name="pay_method" value="cash" id="sizeDimensions" />
                                                        <label for="sizeDimensions" style="outline: none;">{{__('global.cash')}}</label>
                                                    </span>
                                                </p>
                                                <p>
                                                    <textarea name="location_url" id="bill" cols="30" rows="10" placeholder="{{__('placeholder.location_url')}}"></textarea>
                                                </p>
                                                <p>
                                                    <textarea name="message" id="bill" cols="30" rows="10" placeholder="Message"></textarea>
                                                </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
{{--                            <div class="card single-accordion">--}}
{{--                                <div class="card-header" id="headingTwo">--}}
{{--                                    <h5 class="mb-0">--}}
{{--                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">--}}
{{--                                            Shipping Address--}}
{{--                                        </button>--}}
{{--                                    </h5>--}}
{{--                                </div>--}}
{{--                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <div class="shipping-address-form">--}}
{{--                                            <p>Your shipping address form is here.</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="card single-accordion">--}}
{{--                                <div class="card-header" id="headingThree">--}}
{{--                                    <h5 class="mb-0">--}}
{{--                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">--}}
{{--                                            Card Details--}}
{{--                                        </button>--}}
{{--                                    </h5>--}}
{{--                                </div>--}}
{{--                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <div class="card-details">--}}
{{--                                            <p>Your card details goes here.</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>

                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="order-details-wrap">
                        <table class="order-details">
                            <thead>
                            <tr>
                                <th><b>{{__('global.order_details')}}</b></th>
                                <th><b>{{__('global.price')}}</b></th>
                            </tr>
                            </thead>
                            <tbody class="order-details-body">
                            @foreach($shoppings as $shopping)
                                <tr>
                                    <td>{{\App\Hellper\GetLocalizedValue::GetValue($shopping['product'], 'title')}}</td>
                                    <td>{{isset($shopping['product']['sale']) ? $shopping['product']['sale']*$shopping['quantity'] : $shopping['product']['price']*$shopping['quantity']}} {{__('global.amd')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tbody class="checkout-details" style="    border: 2px solid #bdbdbd;">
                            <tr>
                                <td>{{__('global.sub_total')}}</td>
                                <td>{{$sum}} {{__('global.amd')}}</td>
                            </tr>
                            <tr>
                                <td>{{__('global.shipping')}}</td>
                                <td>0 {{__('global.amd')}}</td>
                            </tr>
                            <tr>
                                <td><b>{{__('global.total')}}</b></td>
                                <td><b>{{$sum}} {{__('global.amd')}}</b></td>
                            </tr>
                            </tbody>
                        </table>
                        <a href="javascript:void(0)" class="boxed-btn create-order-checkout">{{__('global.place_order')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end check out section -->


@endsection()
