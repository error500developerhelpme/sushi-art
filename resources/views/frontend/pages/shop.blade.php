@extends('layouts.app')



@section('css')

@endsection()

@section('content')


    <!-- breadcrumb-section -->
    @include('frontend.components.breadcrumb', ['subtitle'=>__('breadcrumb.shop_header_subtitle'), 'title'=>__('breadcrumb.shop_header_title'), 'path'=>'assets/custom/img/IMG_2293.jpg'])
    <!-- end breadcrumb section -->

    <!-- products -->
    <div class="product-section mt-150 mb-150">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="product-filters">
                        <ul>
                            <li class="active" data-filter="*">{{__('global.all')}}</li>
                            @foreach($categories as $category)
                                <li data-filter=".item{{$category->id}}">{{\App\Hellper\GetLocalizedValue::GetValue($category, 'title')}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row product-lists">
               @include('frontend.components.product-card', ['products'=>$products])
            </div>

             @include('frontend.components.paginate', ['type'=> false])
        </div>
    </div>
    <!-- end products -->



@endsection()
