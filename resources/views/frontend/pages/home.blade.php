@extends('layouts.app')



@section('css')

@endsection()

@section('content')

    <!-- home page slider -->
    <div class="homepage-slider">
        <!-- single home slider -->
        @foreach($sliders as $slide)
            <div class="single-homepage-slider " style="background-image: url({{asset($slide->path)}})">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-7 offset-lg-1 offset-xl-0">
                            <div class="hero-text">
                                <div class="hero-text-tablecell">
                                    <p class="subtitle">{{\App\Hellper\GetLocalizedValue::GetValue($slide, 'sub_title')}}</p>
                                    <h1>{{\App\Hellper\GetLocalizedValue::GetValue($slide, 'title')}}</h1>
                                    <div class="hero-btns">
                                        @if(isset($slide->product_id))
{{--                                            <a href="{{route('single.product', $slide->product_id)}}" class="boxed-btn">See Product</a>--}}
                                            <a href="{{route('single.product', $slide->product_id)}}" class="bordered-btn">{{__('button.see_product')}}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <!-- single home slider -->
    </div>
    <!-- end home page slider -->

    <!-- features list section -->
    <div class="list-section pt-80 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                    <div class="list-box d-flex align-items-center">
                        <div class="list-icon">
                            <i class="fas fa-shipping-fast"></i>
                        </div>
                        <div class="content">
                            <h3>{{get_page_model_translate_value($page['shipping']['title'])}}</h3>
                            <p>{{get_page_model_translate_value($page['shipping']['description'])}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                    <div class="list-box d-flex align-items-center">
                        <div class="list-icon">
                            <i class="fas fa-phone-volume"></i>
                        </div>
                        <div class="content">
                            <h3>{{get_page_model_translate_value($page['support']['title'])}}</h3>
                            <p>{{get_page_model_translate_value($page['support']['description'])}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="list-box d-flex justify-content-start align-items-center">
                        <div class="list-icon">
                            <i class="fas fa-sync"></i>
                        </div>
                        <div class="content">
                            <h3>{{get_page_model_translate_value($page['refund']['title'])}}</h3>
                            <p>{{get_page_model_translate_value($page['refund']['description'])}}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- end features list section -->

    <!-- product section -->
    <div class="product-section mt-150 mb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center">
                    <div class="section-title">
                        <h3><span class="orange-text">{{__('home.our')}}</span> {{__('home.products')}}</h3>
                        <p>{{__('home.description_our_products')}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                @include('frontend.components.product-card', ['product' => $products])
            </div>
        </div>
    </div>
    <!-- end product section -->

    <!-- cart banner section -->
    <section class="cart-banner pt-100 pb-100">
        <div class="container">
            <div class="row clearfix">
                <!--Image Column-->
                <div class="image-column col-lg-6">
                    <div class="image">
                        <div class="price-box">
                            <div class="inner-price">
                                <span class="price">
                                    <strong>{{$percentRandom->percent ?? ''}}%</strong>
                                </span>
                            </div>
                        </div>
                        <img src="{{asset(has_file($percentRandom->images[0]->path, 'product'))}}" alt="" style="    border-radius: 50px;">
                    </div>
                </div>
{{--                {{dd($page['section_offer_month'])}}--}}
                <!--Content Column-->
                <div class="content-column col-lg-6">
                    <h3>
                        <span class="orange-text">
                            {{get_page_model_translate_value($page['section_offer_month']['title_orange'])}}
                        </span>
                        {{get_page_model_translate_value($page['section_offer_month']['title_black'])}}
                    </h3>
                    <h4>{{get_page_model_translate_value($page['section_offer_month']['sub_title'])}}</h4>
                    <div class="text">{{get_page_model_translate_value($page['section_offer_month']['description'])}}</div>
                    <!--Countdown Timer-->
                    @if($page['section_offer_month']['timer'])
                        <div class="time-counter">
                            <div class="time-countdown clearfix" data-countdown="2020/2/01">
                                <div class="counter-column">
                                    <div class="inner">
                                        <span class="count">00</span>
                                        Days
                                    </div>
                                </div>
                                <div class="counter-column">
                                    <div class="inner">
                                        <span class="count">00</span>
                                        Hours
                                    </div>
                                </div>
                                <div class="counter-column">
                                    <div class="inner">
                                        <span class="count">00</span>Mins
                                    </div>
                                </div>
                                <div class="counter-column">
                                    <div class="inner">
                                        <span class="count">00</span>
                                        Secs
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @include('frontend.components.button-add-to-card', ['product_id' => $percentRandom->id])
                </div>
            </div>
        </div>
    </section>
    <!-- end cart banner section -->

    <!-- testimonail-section -->
    <div class="testimonail-section mt-150 mb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 text-center">
{{--                    <div class="testimonial-sliders">--}}
{{--                        @foreach($categories as $category)--}}
{{--                            <div class="single-testimonial-slider">--}}
{{--                                <div class="client-avater">--}}
{{--                                    <img src="{{asset('assets/custom/img/sushi/13.jpg')}}" alt="">--}}
{{--                                </div>--}}
{{--                                <div class="client-meta">--}}
{{--                                    <h3>{{\App\Hellper\GetLocalizedValue::GetValue($category, 'title')}}</span></h3>--}}
{{--                                    <p class="testimonial-body">--}}
{{--                                        " contains 8 pieces of rolls, 2 different varieties and also comes with pomegranate "--}}
{{--                                    </p>--}}
{{--                                    <div class="last-icon">--}}
{{--                                        <i class="fas fa-quote-right"></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- advertisement section -->
    <div class="abt-section mb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="abt-bg" style="background-image: url({{asset($page['section_video']['image'])}}); height: 400px;">
                        <!-- Button trigger modal -->
                        <a type="button" class="btn-primary video-btn video-play-btn"
                           data-toggle="modal"
                           data-src="{{asset($page['section_video']['video'])}}"
                           data-target="#videoHomePage">
                            <i class="fas fa-play"></i>
                        </a>
{{--                        <a href="javascript:;" class="video-play-btn popup-youtube"><i class="fas fa-play"></i></a>--}}
                    </div>
                </div>
                <!-- Modal -->
               @include('frontend.components.modal', ['type' => 'videoHomePage'])

                <!-- end search area -->
                <div class="col-lg-6 col-md-12">
                    <div class="abt-text">
                        <p class="top-sub">
                            {{get_page_model_translate_value($page['section_video']['sub_title'])}}
                        </p>
                        <h2>
                            {{get_page_model_translate_value($page['section_video']['title_black'])}}
                            <span class="orange-text">
                                {{get_page_model_translate_value($page['section_video']['title_orange'])}}
                            </span>
                        </h2>
                        <p>
                            {{get_page_model_translate_value($page['section_video']['description'])}}
                        </p>
                        @if($page['section_video']['button'])
                            <a href="{{route('about')}}" class="boxed-btn mt-4">{{__('button.more')}}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end advertisement section -->

    <!-- shop banner -->
    <section class="shop-banner" style="background-image: url({{asset($page['section_sale']['image'])}});">
{{--    <section class="shop-banner" style="background-image: url({{asset(has_file($percent->images[0]->path, 'product'))}});">--}}
        <div class="container">
            <h3 style="color: wheat;">
                {!! get_page_model_translate_value($page['section_sale']['title_black']) !!}
                <span class="orange-text">
                    {{get_page_model_translate_value($page['section_sale']['title_orange'])}}
                </span>
            </h3>
            <div class="sale-percent" >
                <span style="color: wheat;">
                    {{__('global.sale')}} <br> {{__('global.upto')}}
                </span>
                    {{$percent->percent}}%
                <span style="color: wheat;">
                    {{__('global.off')}}
                </span>
            </div>
            <a href="{{route('shop')}}" class="cart-btn btn-lg">{{__('button.shop_now')}}</a>
        </div>
    </section>
    <!-- end shop banner -->

    <!-- latest news -->
    <div class="latest-news pt-150 pb-150">
        <div class="container">

{{--            <div class="row">--}}
{{--                <div class="col-lg-8 offset-lg-2 text-center">--}}
{{--                    <div class="section-title">--}}
{{--                        <h3><span class="orange-text">{{__('home.our')}}</span> News</h3>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, fuga quas itaque eveniet beatae optio.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="row">--}}
{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-latest-news">--}}
{{--                        <a href="{{route('single.news')}}"><div class="latest-news-bg news-bg-1" style="background-image: url({{asset('assets/custom/img/sushi/32.jpg')}});"></div></a>--}}
{{--                        <div class="news-text-box">--}}
{{--                            <h3><a href="{{route('single.news')}}">You will vainly look for fruit on it in autumn.</a></h3>--}}
{{--                            <p class="blog-meta">--}}
{{--                                <span class="author"><i class="fas fa-user"></i> Admin</span>--}}
{{--                                <span class="date"><i class="fas fa-calendar"></i> 27 December, 2019</span>--}}
{{--                            </p>--}}
{{--                            <p class="excerpt">Vivamus lacus enim, pulvinar vel nulla sed, scelerisque rhoncus nisi. Praesent vitae mattis nunc, egestas viverra eros.</p>--}}
{{--                            <a href="{{route('single.news')}}" class="read-more-btn">read more <i class="fas fa-angle-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-latest-news">--}}
{{--                        <a href="{{route('single.news')}}"><div class="latest-news-bg news-bg-2" style="background-image: url({{asset('assets/custom/img/sushi/31.jpg')}});"></div></a>--}}
{{--                        <div class="news-text-box">--}}
{{--                            <h3><a href="{{route('single.news')}}">A man's worth has its season, like tomato.</a></h3>--}}
{{--                            <p class="blog-meta">--}}
{{--                                <span class="author"><i class="fas fa-user"></i> Admin</span>--}}
{{--                                <span class="date"><i class="fas fa-calendar"></i> 27 December, 2019</span>--}}
{{--                            </p>--}}
{{--                            <p class="excerpt">Vivamus lacus enim, pulvinar vel nulla sed, scelerisque rhoncus nisi. Praesent vitae mattis nunc, egestas viverra eros.</p>--}}
{{--                            <a href="{{route('single.news')}}" class="read-more-btn">read more <i class="fas fa-angle-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-6 offset-md-3 offset-lg-0">--}}
{{--                    <div class="single-latest-news">--}}
{{--                        <a href="{{route('single.news')}}"><div class="latest-news-bg news-bg-3" style="background-image: url({{asset('assets/custom/img/sushi/30.jpg')}});"></div></a>--}}
{{--                        <div class="news-text-box">--}}
{{--                            <h3><a href="{{route('single.news')}}">Good thoughts bear good fresh juicy fruit.</a></h3>--}}
{{--                            <p class="blog-meta">--}}
{{--                                <span class="author"><i class="fas fa-user"></i> Admin</span>--}}
{{--                                <span class="date"><i class="fas fa-calendar"></i> 27 December, 2019</span>--}}
{{--                            </p>--}}
{{--                            <p class="excerpt">Vivamus lacus enim, pulvinar vel nulla sed, scelerisque rhoncus nisi. Praesent vitae mattis nunc, egestas viverra eros.</p>--}}
{{--                            <a href="{{route('single.news')}}" class="read-more-btn">read more <i class="fas fa-angle-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-12 text-center">--}}
{{--                    <a href="{{route('news')}}" class="boxed-btn">{{__('button.more')}}</a>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
    <!-- end latest news -->

    <!-- logo carousel -->
    <div class="logo-carousel-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="logo-carousel-inner">
                        <div class="single-logo-item">
                            <img src="{{asset('assets/frontend/custom/images/yandexeat-full.png')}}" alt="Yandex eat" style="border-radius: 70px;">
                        </div>
                        <div class="single-logo-item">
                            <img src="{{asset('assets/frontend/custom/images/glovo-full.png')}}" alt="Glovo" style="border-radius: 70px;">
                        </div>
                        <div class="single-logo-item">
                            <img src="{{asset('assets/frontend/custom/images/menu.png')}}" alt="Menu.am" style="border-radius: 70px;">
                        </div>
                        <div class="single-logo-item">
                            <img src="{{asset('assets/frontend/custom/images/buy.png')}}" alt="Buy.am" style="border-radius: 70px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end logo carousel -->

@endsection()
