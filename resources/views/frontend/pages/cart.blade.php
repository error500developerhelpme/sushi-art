@extends('layouts.app')



@section('css')

    <style>
        .product-image img {
            max-width: 100% !important;
            object-fit: cover;
        }

    </style>
@endsection()

@section('content')


    <!-- breadcrumb-section -->
    @include('frontend.components.breadcrumb', ['subtitle'=>__('breadcrumb.cart_header_subtitle'), 'title'=>__('breadcrumb.cart_header_title'), 'path'=>'assets/custom/img/IMG_2293.jpg'])
    <!-- end breadcrumb section -->

    <!-- cart -->
    <div class="cart-section mt-150 mb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    @include('frontend.components.row-product-card', ['shoppings' => $shoppings, 'sum' => $sum, 'access' => 'true' ])
                </div>

                <div class="col-lg-4">
                    @include('frontend.components.total-section', ['sum' => $sum])

{{--                    <div class="coupon-section">--}}
{{--                        <h3>Apply Coupon</h3>--}}
{{--                        <div class="coupon-form-wrap">--}}
{{--                            <form action="index.html">--}}
{{--                                <p><input type="text" placeholder="Coupon"></p>--}}
{{--                                <p><input type="submit" value="Apply"></p>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- end cart -->


@endsection()
