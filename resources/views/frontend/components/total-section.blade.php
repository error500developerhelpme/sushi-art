<div class="total-section">
    <table class="total-table">
        <thead class="total-table-head">
        <tr class="table-total-row">
            <th>{{__('global.total')}}</th>
            <th>{{__('global.price')}}</th>
        </tr>
        </thead>
        <tbody>
        <tr class="total-data">
            <td><strong>{{__('global.sub_total')}}: </strong></td>
            <td><span class="shopping-card-subtotal" data-sum="{{$sum}}">{{$sum}}</span> {{__('global.amd')}}</td>
        </tr>
        <tr class="total-data">
            <td><strong>{{__('global.shipping')}}: </strong></td>
            <td><span class="shopping-card-shipping">{{$sum > 5000 ? 0 : '~//~'}}</span> {{__('global.amd')}}</td>
        </tr>
        <tr class="total-data">
            <td><strong>{{__('global.total')}}: </strong></td>
            <td><span class="shopping-card-total">{{$sum}}</span> {{__('global.amd')}}</td>
        </tr>
        </tbody>
    </table>
    @if(to_route('cart'))
        <div class="cart-buttons">
            {{--                            <a href="cart.html" class="boxed-btn">Update Cart</a>--}}
            @if($sum == 0)
                <a href="{{route('shop')}}" class="boxed-btn black">{{__('global.check_out')}}</a>
            @else
                @guest()
                    <a href="{{route('guest.checkout')}}" class="boxed-btn black">{{__('global.check_out')}}</a>
                @else
                    <a href="{{route('checkout')}}" class="boxed-btn black">{{__('global.check_out')}}</a>
                @endguest
            @endif
        </div>
    @endif
</div>
