<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
<!-- fontawesome -->
<link rel="stylesheet" href="{{asset('assets/frontend/assets/css/all.min.css')}}">
<!-- bootstrap -->
<link rel="stylesheet" href="{{asset('assets/frontend/assets/bootstrap/css/bootstrap.min.css')}}">
<!-- owl carousel -->
<link rel="stylesheet" href="{{asset('assets/frontend/assets/css/owl.carousel.css')}}">
<!-- magnific popup -->
<link rel="stylesheet" href="{{asset('assets/frontend/assets/css/magnific-popup.cs')}}s">
<!-- animate css -->
<link rel="stylesheet" href="{{asset('assets/frontend/assets/css/animate.css')}}">
<!-- mean menu css -->
<link rel="stylesheet" href="{{asset('assets/frontend/assets/css/meanmenu.min.css')}}">
<!-- main style -->
<link rel="stylesheet" href="{{asset('assets/frontend/assets/css/main.css')}}">
<!-- responsive -->
<link rel="stylesheet" href="{{asset('assets/frontend/assets/css/responsive.css')}}">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />

<link rel="stylesheet" href="{{asset('assets/custom/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/frontend/custom/css/css.css')}}">



<style>
    .style-shipping-card-count{
        padding: 0px 5px;
        border: 2px solid red;
        border-radius: 83px;
        font-size: 12px;
        margin-right: 6px;
    }
    .global-color{
        color: #ff7300 !important;
    }

    .global-color:hover{
        color: #F28123 !important;
    }

</style>
