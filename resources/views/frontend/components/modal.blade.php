@if($type == 'orderAgain')
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="orderAgain" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">{{__('global.order_again')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('order.again')}}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="modal-body">
                        <div class="timeline-body ">
                            <div class="row">
                                <lable class="col-12">
                                    <b>{{__('global.name')}}</b>
                                    <input style="margin-bottom: 10px" class="form-control " type="text" name="name" placeholder="{{__('placeholder.name')}}" value="{{$order->name}}">
                                </lable>
                                <lable class="col-12">
                                    <b>{{__('global.city')}}</b>
                                    <input style="margin-bottom: 10px" class="form-control" type="text" name="city" placeholder="{{__('placeholder.city')}}" value="{{$order->city}}">
                                </lable>
                                <lable class="col-12">
                                    <b>{{__('global.address')}}</b>
                                    <input style="margin-bottom: 10px" class="form-control" type="text" name="address" placeholder="{{__('placeholder.address')}}" value="{{$order->address}}">
                                </lable>
                                <lable class="col-12">
                                    <b>{{__('global.phone')}}</b>
                                    <input style="margin-bottom: 10px" class="form-control" type="text" name="phone" placeholder="{{__('placeholder.phone')}}" value="{{$order->phone}}">
                                </lable>
                                <lable class="col-12">
                                    <b>{{__('placeholder.location_url')}}</b>
                                    <input style="margin-bottom: 10px" class="form-control" type="text" name="location_url" placeholder="{{__('placeholder.location_url')}}" value="{{$order->location_url}}">
                                </lable>
                                <lable class="col-12">
                                    <b>{{__('placeholder.message')}}</b>
                                    <textarea style="margin-bottom: 10px"  class="form-control col-12" name="message" id="" cols="30" rows="10" placeholder="{{__('placeholder.message')}}">{{$order->message}}</textarea>
                                </lable>

                                <lable class="col-12">
                                    <span class="toggle">
                                        <input type="radio" name="pay_method" value="card" id="sizeWeight" checked="checked" />
                                        <label for="sizeWeight" style=" outline: none;">{{__('global.card')}}</label>
                                        <input type="radio" name="pay_method" value="cash" id="sizeDimensions" />
                                        <label for="sizeDimensions" style="outline: none;">{{__('global.cash')}}</label>
                                    </span>
                                </lable>
                            </div>
                            <div style="overflow-x: scroll;">
                                @include('frontend.components.row-product-card', ['shoppings' => $order->card, 'sum' => $order->price, 'access' => 'false' ])
{{--                                <table class="cart-table" style="margin-bottom: 10px" >--}}
{{--                                    <thead class="cart-table-head">--}}
{{--                                    <tr class="table-head-row">--}}
{{--                                        <th class="product-remove"></th>--}}
{{--                                        <th class="product-image">{{__('global.product_image')}}</th>--}}
{{--                                        <th class="product-name">{{__('global.name')}}</th>--}}
{{--                                        <th class="product-price">{{__('global.price')}}</th>--}}
{{--                                        <th class="product-quantity">{{__('global.quantity')}}</th>--}}
{{--                                        <th class="product-total">{{__('global.total')}}</th>--}}
{{--                                    </tr>--}}
{{--                                    </thead>--}}
{{--                                    <tbody>--}}
{{--                                    @foreach($order->card as $key => $item)--}}
{{--                                        <tr class="table-body-row">--}}
{{--                                            <td class="product-remove"><a style="margin-left: 20px;" href="javascript:void(0)" class="remove-shopping-card" data-access="false" data-id="{{$item->id}}" data-sum="{{$order->price}}"  data-price="{{isset($item->product->sale) ? $item->product->sale*$item->quantity : $item->product->price*$item->quantity}}"><i style="color: red;" class="far fa-trash-alt"></i></a></td>--}}
{{--                                            <td class="product-image"><img style="max-width: 50%!important;" src="{{asset(has_file($item->product->images[0]->path, 'product'))}}" alt=""></td>--}}
{{--                                            <td class="product-name">{{\App\Hellper\GetLocalizedValue::GetValue($item->product, 'title')}}</td>--}}
{{--                                            <td class="product-price">{{isset($item->product->sale) ? $item->product->sale : $item->product->price}} {{__('global.amd')}}</td>--}}
{{--                                            <td class="product-quantity"><input class="shopping-card-quantity" name="quantity[{{$item->product->id}}]" type="number" placeholder="0" min="1" value="{{$item->quantity}}" data-access="false" data-id="{{$item->id}}" data-price="{{isset($item->product->sale) ? $item->product->sale : $item->product->price}}"></td>--}}
{{--                                            <td> <span class="product-total shopping-card-product-total{{$item->id}} all-price-total">{{isset($item->product->sale) ? $item->product->sale*$item->quantity : $item->product->price*$item->quantity}} </span> {{__('global.amd')}}</td>--}}
{{--                                        </tr>--}}
{{--                                    @endforeach--}}
{{--                                    </tbody>--}}
{{--                                </table>--}}
                            </div>

                            <br>
                            <div class="col-lg-12" style="padding: 0px;">
                                @include('frontend.components.total-section', ['sum' => $order->price])
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('button.close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('button.save_and_submit')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@elseif($type == 'videoHomePage')
    <div class="modal fade" id="videoHomePage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="embed-responsive embed-responsive-16by9">
                        <video width="400" controls autoplay id="video"></video>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif


