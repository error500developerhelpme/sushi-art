<div class="breadcrumb-section breadcrumb-bg" style="background-image: url({{asset($path)}})">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <div class="breadcrumb-text">
                    <p>{{$subtitle}}</p>
                    <h1>{{$title}}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
