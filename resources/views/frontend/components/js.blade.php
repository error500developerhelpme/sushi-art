<!-- jquery -->
<script src="{{asset('/assets/frontend/assets/js/jquery-1.11.3.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('/assets/frontend/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- count down -->
<script src="{{asset('/assets/frontend/assets/js/jquery.countdown.js')}}"></script>
<!-- isotope -->
<script src="{{asset('/assets/frontend/assets/js/jquery.isotope-3.0.6.min.js')}}"></script>
<!-- waypoints -->
<script src="{{asset('/assets/frontend/assets/js/waypoints.js')}}"></script>
<!-- owl carousel -->
<script src="{{asset('/assets/frontend/assets/js/owl.carousel.min.js')}}"></script>
<!-- magnific popup -->
<script src="{{asset('/assets/frontend/assets/js/jquery.magnific-popup.min.js')}}"></script>
<!-- mean menu -->
<script src="{{asset('/assets/frontend/assets/js/jquery.meanmenu.min.js')}}"></script>
<!-- sticker js -->
<script src="{{asset('/assets/frontend/assets/js/sticker.js')}}"></script>
<!-- main js -->
<script src="{{asset('/assets/frontend/assets/js/main.js')}}"></script>

{{--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>--}}

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>

<script src="{{asset('/assets/frontend/custom/js/shippingCardOrder.js')}}"></script>
<script src="{{asset('/assets/frontend/custom/js/js.js')}}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

