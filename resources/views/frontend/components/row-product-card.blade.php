<style>
    .product-remove,.product-image,.product-name,.product-price,.product-quantity {
        padding: 10px !important;
    }
</style>
<div class="cart-table-wrap">
    <table class="cart-table">
        <thead class="cart-table-head">
        <tr class="table-head-row">
            <th class="product-remove"></th>
            <th class="product-image">{{__('global.product_image')}}</th>
            <th class="product-name">{{__('global.name')}}</th>
            <th class="product-price">{{__('global.price')}}</th>
            <th class="product-quantity">{{__('global.quantity')}}</th>
            <th class="product-total">{{__('global.total')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($shoppings as $shopping)
            <tr class="table-body-row">
                <td class="product-remove">
                    <a
                        href="javascript:void(0)"
                        class="remove-shopping-card"
                        data-access="{{$access}}"
                        data-user="{{auth()->id()}}"
                        data-id="{{$shopping['id']}}"
                        data-product="{{$shopping['product']['id']}}"
                        data-price="{{isset($shopping['product']['sale']) ? $shopping['product']['sale']*$shopping['quantity'] : $shopping['product']['price']*$shopping['quantity']}}"
                    >
                        <i style="color: red;" class="far fa-trash-alt"></i>
                    </a>
                </td>
                <td class="product-image">
                    <img
                        style="max-width: 100% !important;object-fit: cover;"
                        src="{{asset(has_file($shopping['product']['images'][0]['path'], 'product'))}}"
                        alt="">
                </td>
                <td class="product-name">
                    {{\App\Hellper\GetLocalizedValue::GetValue($shopping['product'], 'title')}}
                </td>
                <td class="product-price">
                    {{isset($shopping['product']['sale']) ? $shopping['product']['sale'] : $shopping['product']['price']}} {{__('global.amd')}}
                </td>
                <td class="product-quantity">
                    <input
                        class="shopping-card-quantity"
                        type="number"
                        placeholder="0"
                        min="1"
                        value="{{$shopping['quantity']}}"
                        data-access="{{$access}}"
                        data-id="{{$shopping['id']}}"
                        data-user="{{auth()->id() ?? null}}"
                        data-product="{{$shopping['product']['id']}}"
                        data-quantity="{{$shopping['quantity']}}"
                        data-price="{{isset($shopping['product']['sale']) ? $shopping['product']['sale'] : $shopping['product']['price']}}"
                        name="quantity[{{$shopping['product']['id']}}]"
                    >
                </td>
                <td class="product-sum-total">
                    <span class="product-total shopping-card-product-total{{$shopping['id']}} all-price-total">
                        {{isset($shopping['product']['sale']) ? $shopping['product']['sale']*$shopping['quantity'] : $shopping['product']['price']*$shopping['quantity']}}
                    </span>
                    {{__('global.amd')}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
