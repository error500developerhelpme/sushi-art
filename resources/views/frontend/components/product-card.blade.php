@foreach($products as $product)
    <div class="col-lg-4 col-md-6 text-center item{{$product->category_id}}">
        <div class="single-product-item" style="height: 100%;">
            <div class="product-image">
                <a href="{{route('single.product',$product->id )}}"><img src="{{asset(has_file($product->images[0]->path, 'product'))}}" alt=""></a>
            </div>
            <h3>{{ \App\Hellper\GetLocalizedValue::GetValue($product, 'title')  }}</h3>
            @if(isset($product->sale))
                <p class="product-price"><span><del>{{$product->price}}</del> {{__('global.amd')}}</span> {{$product->sale}} {{__('global.amd')}}</p>
            @else
                <p class="product-price" style="margin-top: 37px;"> {{$product->price}} {{__('global.amd')}}</p>
            @endif
            @include('frontend.components.button-add-to-card', ['product_id' => $product->id])
        </div>
    </div>
@endforeach
