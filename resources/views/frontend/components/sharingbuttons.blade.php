<h4>{{__('global.search')}}:</h4>

<ul class="product-share">
    <li><a href="https://facebook.com/sharer/sharer.php?u={{url()->full()}}"><i class="fab fa-facebook-f"></i></a></li>
    <li><a href="https://twitter.com/intent/tweet/?text={{urlencode($message)}}&amp;url={{urlencode(url()->full())}}"><i class="fab fa-twitter"></i></a></li>
    <li><a href="https://www.instagram.com/?text={{urlencode($message)}}&amp;url={{urlencode(url()->full())}}"><i class="fab fa-instagram"></i></a></li>
{{--    <li><a href="https://www.instagram.com/?url={{urlencode(url()->full())}}"><i class="fab fa-instagram"></i></a></li>--}}
    <li><a href="whatsapp://send?text={{urlencode($message)}}%20{{urlencode(url()->full())}}"><i class="fab fa-whatsapp"></i></a></li>
    <li><a href="https://telegram.me/share/url?text={{urlencode($message)}}&amp;url={{urlencode(url()->full())}}"><i class="fab fa-telegram"></i></a></li>
</ul>

