@auth()
    @if(auth()->user()->block)
        <div
            style="width: 100%;height: 20px;background-color: red;z-index: 11111;position: fixed;">
            <p
                style="color: white;text-align: center;font-size: 16px;margin-top: -5px;font-weight: 600;">
                {{__('global.account_blocked')}}
            </p>
        </div>
    @endif
@endauth
