<!-- header -->
<div class="top-header-area" id="sticker">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <div class="main-menu-wrap">
                    <!-- logo -->
                    <div class="site-logo">
                        <a href="{{route('home.page')}}">
                            <img src="{{asset('assets/custom/img/ART_V6.png')}}" alt="">
                        </a>
                    </div>
                    <!-- logo -->
{{--                    {{App::setLocale('en'),  dd(App::getLocale())}}--}}
                    <!-- menu start -->
                    <nav class="main-menu">
                        <ul>
                            <li class="{{Route::currentRouteNamed('home.page') ? 'current-list-item' : ''}}"><a href="{{route('home.page')}}">{{__('navbar.home')}}</a></li>
                            <li class="{{Route::currentRouteNamed('shop') ? 'current-list-item' : ''}}"><a href="{{route('shop')}}">{{__('navbar.shop')}}</a></li>
                            <li class="{{Route::currentRouteNamed('contact') ? 'current-list-item' : ''}}" ><a href="{{route('contact')}}">{{__('navbar.contact')}}</a></li>
{{--                            <li><a href="{{route('shop')}}">Shop</a>--}}
{{--                                <ul class="sub-menu">--}}
{{--                                    <li><a href="{{route('shop')}}">Shop</a></li>--}}
{{--                                    <li><a href="{{route('checkout')}}">Check Out</a></li>--}}
{{--                                    <li><a href="{{route('cart')}}">Cart</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}


                            <li>
                                <a href="javascript:void(0);" class="mobile-and-display">
                                    <span>
                                        {{ \App\Hellper\GetLocalizedValue::GetSelectLang(session('lang'))['accesser']->name }}
                                    </span>
                                    <img class="img-lang" src="{{ \App\Hellper\GetLocalizedValue::GetSelectLang(session('lang'))['country']->path }}" alt="Lang">
                                </a>
                                <ul class="sub-menu">
                                    @foreach($countriesGLOBAL as $country)
                                        <li>
                                            <a  class="lang" href="{{route('setlocale', ['lang' => $country->lang ])}}" style="display: flow-root; @if($country->lang === session('lang')) background: #c1c1c17a; border-radius: 10px; @endif">
                                                <img class="img-lang" src="{{ $country->path }}" alt="" style="float: left;width: 12%;">
                                                <div style="float: right; width: 60%;">{{ \App\Hellper\GetLocalizedValue::GetValue($country, 'name') }}</div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <div class="header-icons">
                                    @if(auth()->id())
                                        <a class="shopping-cart" href="{{route('cart')}}">
                                            <span class="style-shipping-card-count" @if($shoppingCardGLOBAL == 0 )style="display: none"@endif >
                                                {{$shoppingCardGLOBAL}}
                                            </span>
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    @else
                                        <a class="shopping-cart" href="{{route('guest.cart', 55)}}">
                                            <span class="style-shipping-card-count cookie-count-product "></span>
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    @endif
                                    <a class="mobile-hide search-bar-icon" href="#"><i class="fas fa-search"></i></a>
                                    @guest()
                                        <a class="shopping-cart" href="{{route('login')}}"><i class="fa fa-user" aria-hidden="true"></i></a>
                                    @else
                                        @if(auth()->user()->type == 'admin')
                                            <a class="shopping-cart" href="{{route('dashboard')}}"><i class="fa fa-user" aria-hidden="true"></i></a>
                                            @else
                                            <a class="shopping-cart" href="{{route('home')}}"><i class="fa fa-user" aria-hidden="true"></i></a>
                                        @endif
                                    @endguest
                                </div>
                            </li>

                        </ul>
                    </nav>
                    <a class="mobile-show search-bar-icon" href="#"><i class="fas fa-search"></i></a>
                    <div class="mobile-menu"></div>
                    <!-- menu end -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end header -->

<!-- search area -->
<div class="search-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <span class="close-btn"><i class="fas fa-window-close"></i></span>
                <div class="search-bar">
                    <div class="search-bar-tablecell">
                        <form method="POST" action="{{route('search')}}">
                            @csrf
                            <input type="text" name="search" placeholder="{{__('placeholder.search')}}" style=" outline: none;">
                            <button type="submit">{{__('global.search')}} <i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end search area -->
