<!-- footer -->
<div class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="footer-box about-widget">
                    <h2 class="widget-title">{{__('global.about_us')}}</h2>
                    <p>
                        {{get_page_model_translate_value($pageGLOBAL['about']['description'])}}
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="footer-box get-in-touch">
                    <h2 class="widget-title">{{__('global.footer_title_link')}}</h2>
                    <ul>
                        <li>{{__('global.footer_sub_title_link')}}</li>
                        <li><a href="{{$pageGLOBAL['contact_info']['facebook'] ?? ''}}"><i class="fab fa-facebook-f"></i> Facebook</a></li>
                        <li><a href="{{$pageGLOBAL['contact_info']['instagram'] ?? ''}}"><i class="fab fa-instagram"></i> Instagram</a></li>
                        <li><a href="mailto:{{$pageGLOBAL['contact_info']['email']}}" class="global-color">{{$pageGLOBAL['contact_info']['email']}}</a></li>
                        <li><a href="tel:{{$pageGLOBAL['contact_info']['phone']}}" class="global-color">+{{$pageGLOBAL['contact_info']['phone']}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="footer-box pages">
                    <h2 class="widget-title">{{__('global.footer_page')}}</h2>
                    <ul>
                        <li><a href="{{route('home.page')}}">{{__('navbar.home')}}</a></li>
{{--                        <li><a href="{{route('about')}}">About</a></li>--}}
                        <li><a href="{{route('shop')}}">{{__('navbar.shop')}}</a></li>
{{--                        <li><a href="{{route('news')}}">News</a></li>--}}
                        <li><a href="{{route('contact')}}">{{__('navbar.contact')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="footer-box subscribe">
                    <h2 class="widget-title">{{__('global.footer_subscribe')}}</h2>
                    <p>{{__('global.footer_subscribe_text')}}</p>
                    <form method="POST" action="{{route('subscribe')}}">
                        @method('POST')
                        @csrf
                        <input type="email" name="email" placeholder="{{__('placeholder.email')}}">
                        <button type="submit"><i class="fas fa-paper-plane"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end footer -->

<!-- copyright -->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <p>
                    {{__('global.footer_text')}} <a href="javascript:void(0)">2023</a>
                </p>
            </div>
            <div class="col-lg-6 text-right col-md-12">
                <div class="social-icons">
                    <style>
                        ul.product-share li a{
                            color: #3b5c69;
                        }
                    </style>
                    @include('frontend.components.sharingbuttons', ['message'=>'https://sushiart.am/'])
{{--                    <ul>--}}
{{--                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>--}}
{{--                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>--}}
{{--                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>--}}
{{--                        <li><a href="#" target="_blank"><i class="fab fa-linkedin"></i></a></li>--}}
{{--                        <li><a href="#" target="_blank"><i class="fab fa-dribbble"></i></a></li>--}}
{{--                    </ul>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end copyright -->
