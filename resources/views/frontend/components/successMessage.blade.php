@if ($message = Session::get('success'))
    <div
        style="width: 100%;height: 20px;background-color: #01e301;z-index: 11111;position: fixed;">
        <p
            style="color: white;text-align: center;font-size: 16px;margin-top: -5px;font-weight: 600;">
            {{$message}}
        </p>
    </div>
@endif


@if ($message = Session::get('error'))
    <div
        style="width: 100%;height: 20px;background-color: red;z-index: 11111;position: fixed;">
        <p
            style="color: white;text-align: center;font-size: 16px;margin-top: -5px;font-weight: 600;">
            {{$message}}
        </p>
    </div>
@endif
