@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style>
        span.select2-selection.select2-selection--single{
            height: 40px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow{
            height: 40px;
        }
        .note-editable.card-block{
            height: 300px;
        }
    </style>
@endsection

@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('product.index')}}">Slider</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Edit</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('product.update', $product->id)}}" method="POST" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                                    @include("backend.components.translate")
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-two-tabContent">
                                            @foreach($countriesGLOBAL as $key => $item)
                                                <div class="tab-pane fade  @if($key == 0) show active @endif" id="{{$item->lang}}" role="tabpanel" aria-labelledby="custom-tabs-two-{{$item->lang}}-tab">
                                                    <div class="card-body row">
                                                        <div class="form-group col-3">
                                                            <label for="exampleInputPassword1">Title</label>
                                                            <input type="text" class="form-control" name="langs[{{ $item->lang }}][title]" id="exampleInputPassword1" placeholder="Name" value="{{isset($product->localizations[$key]->title) ? $product->localizations[$key]->title : ''}}">
                                                        </div>
                                                        <div class="form-group col-12">
                                                            <label for="exampleInputPassword1">Description</label>
                                                            {{--summernote--}}
                                                            <textarea class="textarea" name="langs[{{ $item->lang }}][description]" placeholder="Place some description here" >{!! isset($product->localizations[$key]->description) ? $product->localizations[$key]->description : '' !!}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="card-body row">
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Category</label>
                                            <select name="category_id" id="" class="form-control select2">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" @if($category->id == $product->category_id) selected @endif>{{\App\Hellper\GetLocalizedValue::GetValue($category, 'title')}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-3">
                                            <label for="exampleInputPassword1">Price</label>
                                            <input type="number" class="form-control" name="price"  placeholder="Price" value="{{ $product->price }}"  >
                                        </div>
                                        <div class="form-group col-3">
                                            <label for="exampleInputPassword1">Sale</label>
                                            <input type="number" class="form-control" name="sale" placeholder="Sale" value="{{ $product->sale }}"  >
                                        </div>
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">First Images</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="images[]" id="imgfilesOne">
                                                    <label class="custom-file-label" for="imgfilesOne">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view-one">

                                        </div>

                                        <div class="row">
                                            @foreach($product->images as $image)
                                                <div class="col-4 image" data-id="{{ $image->id }}">
                                                    <div class="position-relative">
                                                        @if(count($product->images) > 1)
                                                            <button type="button" data-id="{{ $image->id }}" class="del-clic close position-absolute d-flex align-items-center mr-2" style="right: 0; top: 0; height: auto" aria-label="Close">
                                                                <span style="color: white !important;" aria-hidden="true">&times;</span>
                                                            </button>
                                                        @endif
                                                        <img src="{{asset($image->path)}}" alt="" style="width: 100%;margin-bottom: 15px"> <br>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary story-submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')


@endsection


