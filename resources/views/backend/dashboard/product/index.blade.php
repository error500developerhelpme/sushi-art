@extends('layouts.dashboard')


@section('title')
    <title>Product</title>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

    <style>
        .scroll-image{
            overflow-y: scroll;
            display: inline-flex;
            align-items: center;
        }
        .scroll-image img{
            margin: 5px;
            width: 60%;
        }

        .scroll-text{
            overflow: auto;
            height: 150px;
            display: -webkit-box;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Country</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Product</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div>
                                <a href="{{route('product.create')}}" class="btn btn-success" style="float: right; margin: 15px">Create</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Price/Sale</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Access</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $key => $item)
                                        <tr>
                                            <td>
                                                <img src="{{asset($item->images[0]->path)}}" alt="" style="width: 100%;">
                                            </td>
{{--                                            <td class="scroll-image">--}}
{{--                                                @foreach($item->images as $image)--}}
{{--                                                    <img src="{{asset($image->path)}}" alt="">--}}
{{--                                                @endforeach--}}
{{--                                            </td>--}}
                                            <td>{{ \App\Hellper\GetLocalizedValue::GetValue($item->category, 'title') }}</td>
                                            <td>{{ $item->price }} AMD / {{ $item->sale }} AMD</td>
                                            <td>{{ \App\Hellper\GetLocalizedValue::GetValue($item, 'title') }}</td>
                                            <td class="scroll-text">{!! \App\Hellper\GetLocalizedValue::GetValue($item, 'description') !!}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("product.edit", $item->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-edit" ></i></a>
                                                    <form method="POST" action="{{ route("product.destroy" , $item->id) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <a href="javascript:;" onclick="return confirm('Are you sure you want to delete this item?');">
                                                            <button type="submit" style="padding: 0px 5px;" class="ml-2 btn btn-danger" >
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </a>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            {{ $products->links('pagination::bootstrap-4') }}
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')


@endsection
