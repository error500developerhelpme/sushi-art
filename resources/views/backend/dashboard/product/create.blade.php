@extends('layouts.dashboard')


@section('title')
    <title>Create</title>
@endsection

@section('css')


@endsection



@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('product.index')}}">Product</a></li>
                                <li class="breadcrumb-item active">Create</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Create</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('product.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf

                                    @include("backend.components.translate")
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-two-tabContent">
                                            @foreach($countriesGLOBAL as $key => $item)
                                                <div class="tab-pane fade  @if($key == 0) show active @endif" id="{{$item->lang}}" role="tabpanel" aria-labelledby="custom-tabs-two-{{$item->lang}}-tab">
                                                    <div class="card-body row">
                                                        <div class="form-group col-3">
                                                            <label for="exampleInputPassword1">Title {{$item->lang}}</label>
                                                            <input type="text" class="form-control" name="langs[{{ $item->lang }}][title]" id="exampleInputPassword1" placeholder="Title" value="{{ old('langs['.$item->lang.'][title]') }}"  >
                                                        </div>
                                                        <div class="form-group col-12">
                                                            <label for="exampleInputPassword1">Description</label>
                                                            {{--summernote--}}
                                                            <textarea class="textarea" name="langs[{{ $item->lang }}][description]" placeholder="Place some description here" >{!! old('langs['.$item->lang.'][description]') !!}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="card-body row">
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Category</label>
                                            <select name="category_id" id="" class="form-control select2">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{\App\Hellper\GetLocalizedValue::GetValue($category, 'title')}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-3">
                                            <label for="exampleInputPassword1">Price</label>
                                            <input type="number" class="form-control" name="price"  placeholder="Price" value="{{ old('price') }}"  >
                                        </div>
                                        <div class="form-group col-3">
                                            <label for="exampleInputPassword1">Sale</label>
                                            <input type="number" class="form-control" name="sale" placeholder="Sale" value="{{ old('sale') }}"  >
                                        </div>
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">First Images</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="images[]" id="imgfilesOne" multiple>
                                                    <label class="custom-file-label" for="imgfilesOne">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view-one">

                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary story-submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')

@endsection


