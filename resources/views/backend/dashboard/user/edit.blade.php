@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
@endsection

@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('user.index')}}">User</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Edit</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('user.update', $user->id)}}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <div class="card-body row">
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword1">Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $user->name }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Email</label>
                                            <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $user->email }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword3">Password</label>
                                            <input type="text" class="form-control" name="password" placeholder="password" value="{{ old('password') }}"  >
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary story-submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')

@endsection


