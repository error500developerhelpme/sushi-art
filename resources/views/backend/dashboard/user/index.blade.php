@extends('layouts.dashboard')


@section('title')
    <title>Company</title>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

    <style>
        .show-block-true {
            color: white;
            background: red;
            padding: 5px;
            border-radius: 5px;
            text-align: center;
            display: grid;
        }
        .show-unblock-false {
            color: black;
            background: rgba(128, 128, 128, 0.43);
            padding: 5px;
            border-radius: 5px;
            text-align: center;
            display: grid;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Users</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div>
                                <a href="{{route('user.create')}}" class="btn btn-success" style="float: right; margin: 15px">Create</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <th>Block</th>
                                        <th>Access</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key => $item)
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->type }}</td>
                                            <td>
                                                @if(!$item->block)
                                                    <span class="show-unblock-false"> Unblocked </span>
                                                @else
                                                    <span class="show-block-true"> Blocked </span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("user.edit", $item->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-edit" ></i></a>
                                                    @if(!$item->block)
                                                        <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("user.show", $item->id) }}" class="btn btn-primary edit-access"><i  class="fa-solid fa-lock" ></i></a>
                                                    @else
                                                        <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("user.show", $item->id) }}" class="btn btn-primary edit-access"><i  class="fa-solid fa-unlock" ></i></a>
                                                    @endif
                                                    <form method="POST" action="{{ route("user.destroy" , $item->id) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <a href="javascript:;" onclick="return confirm('Are you sure you want to delete this item?');">
                                                            <button type="submit" style="padding: 0px 5px;" class="ml-2 btn btn-danger" >
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </a>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            {{$users->appends(Request::except('page'))->links('pagination::bootstrap-4')}}
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')


@endsection
