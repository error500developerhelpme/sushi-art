@extends('layouts.dashboard')


@section('title')
    <title>Company</title>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

    <style>
        .show-block-true {
            color: white;
            background: red;
            padding: 5px;
            border-radius: 5px;
            text-align: center;
            display: grid;
        }
        .show-unblock-false {
            color: black;
            background: rgba(128, 128, 128, 0.43);
            padding: 5px;
            border-radius: 5px;
            text-align: center;
            display: grid;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Subscribe</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Subscribe</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Email</th>
{{--                                        <th>Access</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subscribes as $key => $item)
                                        <tr>
                                            <td>{{ $item->email }}</td>
{{--                                            <td>--}}
{{--                                                <div class="d-flex">--}}
{{--                                                    <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("subscribe.send", $item->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-envelope" ></i></a>--}}
{{--                                                </div>--}}
{{--                                            </td>--}}
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')


@endsection
