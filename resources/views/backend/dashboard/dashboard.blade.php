@extends('layouts.dashboard')


@section('title')
    <title>Dashboard</title>
@endsection

@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="file-drel"></div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')

    <script>
        FileDrel(
            [
                {
                    name: 'drel',
                    multiple: true,
                    label: 'Upload a file',
                    // option: ['video', 'image', 'audio']
                },
                {
                    name: 'drel1',
                    multiple: true,
                    label: 'Upload a file111',
                }
            ]
        )
    </script>
@endsection


