@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <style>
        .image-order-admin {
            height: 200px;
            background-repeat: no-repeat;
            background-position: center;
            margin: 5px;
            background-size: 100%;
            padding: 0;
        }
    </style>
@endsection

@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('order.index')}}">Order</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Edit</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('order.update', $order->id)}}" method="POST" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                                    <div class="card-body row">
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $order->name }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">City</label>
                                            <input type="text" class="form-control" name="city" placeholder="Address" value="{{ $order->city }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Address</label>
                                            <input type="text" class="form-control" name="address" placeholder="Address" value="{{ $order->address }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Phone</label>
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ $order->phone }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Shipping</label>
                                            <input type="number" class="form-control" name="shipping" placeholder="Shipping" value="{{ $order->shipping }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Location URL</label>
                                            <input type="text" class="form-control" name="location_url" placeholder="Location URL" value="{{ $order->location_url }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Note</label>
                                            <textarea type="text" class="form-control" name="message" placeholder="Note" >{{ $order->message }}</textarea>
                                        </div>
                                        <lable class="col-12">
                                            <span class="toggle">
                                                <input type="radio" name="pay_method" value="card" id="sizeWeight" checked="checked" />
                                                <label for="sizeWeight" style=" outline: none;">Card</label>
                                                <input type="radio" name="pay_method" value="cash" id="sizeDimensions" />
                                                <label for="sizeDimensions" style="outline: none;">Cash</label>
                                            </span>
                                        </lable>

                                        <div class="form-group" style="display: contents;">
                                            @foreach($products as $product)
                                                <div class="col-12 col-sm-4">
                                                    <div class="info-box bg-light" @if(isset($cards[$product->id])) style="background-color: #00c05426 !important;" @else  @endif>
                                                        <div class="info-box-content">
                                                            <span class="info-box-text text-center text-muted image-order-admin" style="background-image: url({{asset($product->images[0]->path)}})"></span>
                                                            <br>
                                                            <span class="info-box-text text-center text-muted">{{\App\Hellper\GetLocalizedValue::GetValue($product, 'title')}}</span>
                                                            <br>
                                                            <span class="info-box-number text-center text-muted mb-0">Quantity: <input type="number" name="quantity[{{$product->id}}]" value="{{$cards[$product->id] ?? ''}}"></span>
                                                            <br>
                                                            @if(isset($cards[$product->id]) && count($cards) > 1)
                                                                <a href="javascript:void(0)" class="delete-card-order" data-product="{{$product->id}}"  data-order="{{$order->id}}"><i style="color: red;" class="far fa-trash-alt"></i></a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary story-submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')

    <script>
        $('body').on('click', '.delete-card-order', function () {

            let data = {
                'order_id': $(this).attr('data-order'),
                'product_id': $(this).attr('data-product'),
            };
            let classThis = $(this);
            $.ajax({
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/admin/order/card/delete',
                data: data,
                dataType: 'json',
                success: (res) => {
                    classThis.closest('.info-box').attr('style', '')
                    classThis.closest('.info-box').find('.delete-card-order').html('')
                    classThis.closest('.info-box').find('.info-box-number input').val('')
                    if($('.delete-card-order i').length == 1){
                        $('.delete-card-order').html('')
                    }
                },
                error: (data) => {
                    alert("OOP ERROR");
                }
            });

        });
    </script>


@endsection


