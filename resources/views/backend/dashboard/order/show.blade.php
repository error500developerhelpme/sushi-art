@extends('layouts.dashboard')


@section('title')
    <title>Show</title>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <style>
        .image-order-admin {
            height: 200px;
            background-repeat: no-repeat;
            background-position: center;
            margin: 5px;
            background-size: 100%;
            padding: 0;
        }
    </style>
@endsection

@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('order.index')}}">Order</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Count order this user - [ {{$orders}} ]</h3>
                                    <h3 class="card-title" style="float: right;"> Date: {{\Carbon\Carbon::make($order->created_at)->format('d-m-Y H:m A')}}</h3>
                                </div>
                                <div class="card-body row">
                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Name</label>
                                        <input type="text" class="form-control" placeholder="Name" disabled value="{{ $order->name }}"  >
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Phone</label>
                                        <input type="text" class="form-control" placeholder="Phone" disabled value="{{ $order->phone }}"  >
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Address</label>
                                        <input type="text" class="form-control" placeholder="Address" disabled value="{{ $order->address }}"  >
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Price</label>
                                        <input type="text" class="form-control" placeholder="Address" disabled value="{{ $order->price }} AMD "  >
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Pay method</label>
                                        <input type="text" class="form-control" placeholder="Address" disabled value="{{$order->pay_method}}"  >
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Shipping</label>
                                        <input type="text" class="form-control" placeholder="Address" disabled value="{{ $order->shipping }} AMD "  >
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Count all orders</label>
                                        <input type="text" class="form-control" placeholder="Address" disabled value="{{$orders}}"  >
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Location</label>
                                        <textarea type="text" class="form-control" placeholder="Address" disabled style="height: 100px;">{{ $order->location_url }}</textarea>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="exampleInputPassword2">Message</label>
                                        <textarea type="text" class="form-control" placeholder="Address" disabled style="height: 100px;">{{ $order->message }}</textarea>
                                    </div>
                                </div>

                                <form action="{{route('order.status', $order->id)}}" method="POST">
                                    @csrf
                                    @method('POST')
                                    <div class="card-body row">
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Status</label>
                                            <select name="status" id=""  class="form-control select2" style="color: #0b0b0b">
                                                <option value="pending" @if($order->status == 'pending') selected @endif> Pending </option>
                                                <option value="in_process" @if($order->status == 'in_process') selected @endif> In process </option>
                                                <option value="done" @if($order->status == 'done') selected @endif> Done </option>
                                                <option value="sent" @if($order->status == 'sent') selected @endif> Sent </option>
                                                <option value="finished" @if($order->status == 'finished') selected @endif> Finished </option>
                                            </select>

                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Price/Sale</label>
                                            <input type="text" class="form-control" name="sale" placeholder="Sale" value="{{ $order->sale ?? '' }}"  >
                                        </div>
                                        <button type="submit" class="btn btn-primary story-submit" style="margin-top: 5px">Change</button>
                                    </div>
                                </form>

                                <div class="form-group col-12">
                                    <label for="exampleInputPassword2">Product</label>
                                    <div class="row">
                                        @foreach($order->card as $card)
                                            <div class="col-12 col-sm-4">
                                                <div class="info-box bg-light">
                                                    <div class="info-box-content">
                                                        <span class="info-box-text text-center text-muted image-order-admin" style="background-image: url({{asset($card->product->images[0]->path ?? '')}})"></span>
                                                        <br>
                                                        <span class="info-box-text text-center text-muted">{{\App\Hellper\GetLocalizedValue::GetValue($card->product, 'title')}}</span>
                                                        <br>
                                                        <span class="info-box-number text-center text-muted mb-0">Quantity: {{$card->quantity}}</span>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                </div>
                <!-- /.row -->
        </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')

@endsection


