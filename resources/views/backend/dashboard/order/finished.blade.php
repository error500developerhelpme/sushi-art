@extends('layouts.dashboard')


@section('title')
    <title>Order</title>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

    <style>
        .true-status{
            color: green;
        }
        .false-status{
            color: red;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Order</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Order</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div>
{{--                                <a href="{{route('order.create')}}" class="btn btn-success" style="float: right; margin: 15px">Create</a>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Address</th>
                                        <th>Price/Sale</th>
                                        <th>Pay method</th>
                                        <th>Status</th>
                                        <th>Access</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $key => $item)
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->phone }}</td>
                                            <td>{{ $item->city }}, {{$item->address}}</td>
                                            <td>@if(isset($item->sale)){{ $item->sale }} AMD / <del>{{$item->price}}</del> @else {{$item->price}}  @endif AMD</td>
                                            <td>{{ $item->pay_method }}</td>
                                            <td>{{ $item->status }}</td>
                                            <td>
                                                <div class="d-flex">
{{--                                                    <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("order.edit", $item->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-edit" ></i></a>--}}
                                                    <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("order.show", $item->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-eye" ></i></a>
{{--                                                    <form method="POST" action="{{ route("order.destroy" , $item->id) }}">--}}
{{--                                                        @csrf--}}
{{--                                                        @method('DELETE')--}}
{{--                                                        <a href="javascript:;" onclick="return confirm('Are you sure you want to delete this item?');">--}}
{{--                                                            <button type="submit" style="padding: 0px 5px;" class="ml-2 btn btn-danger" >--}}
{{--                                                                <i class="fa fa-trash"></i>--}}
{{--                                                            </button>--}}
{{--                                                        </a>--}}
{{--                                                    </form>--}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            {{$orders->appends(Request::except('page'))->links('pagination::bootstrap-4')}}
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')


@endsection
