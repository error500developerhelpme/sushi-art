@extends('layouts.dashboard')


@section('title')
    <title>Create</title>
@endsection

@section('css')

    <style>
        li.select2-selection__choice {
            color: #0b0b0b;
        }
    </style>
    <style>
        .image-order-admin {
            height: 200px;
            background-repeat: no-repeat;
            background-position: center;
            margin: 5px;
            background-size: 100%;
            padding: 0;
        }
    </style>
@endsection



@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('order.index')}}">Order</a></li>
                                <li class="breadcrumb-item active">Create</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Create</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('order.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body row">
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">City</label>
                                            <input type="text" class="form-control" name="city" placeholder="Address" value="{{ old('city') }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Address</label>
                                            <input type="text" class="form-control" name="address" placeholder="Address" value="{{ old('address') }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Phone</label>
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ old('phone') }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Shipping</label>
                                            <input type="number" class="form-control" name="shipping" placeholder="Shipping" value="{{ old('shipping') }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Location URL</label>
                                            <input type="text" class="form-control" name="location_url" placeholder="Location URL" value="{{ old('location_url') }}"  >
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="exampleInputPassword2">Note</label>
                                            <textarea type="text" class="form-control" name="message" placeholder="Note" >{{ old('message') }}</textarea>
                                        </div>
                                        <lable class="col-12">
                                            <span class="toggle">
                                                <input type="radio" name="pay_method" value="card" id="sizeWeight" checked="checked" />
                                                <label for="sizeWeight" style=" outline: none;">Card</label>
                                                <input type="radio" name="pay_method" value="cash" id="sizeDimensions" />
                                                <label for="sizeDimensions" style="outline: none;">Cash</label>
                                            </span>
                                        </lable>


                                        <div class="form-group" style="display: contents;">
                                            @foreach($products as $product)
                                                <div class="col-12 col-sm-4">
                                                    <div class="info-box bg-light">
                                                        <div class="info-box-content">
                                                            <span class="info-box-text text-center text-muted image-order-admin" style="background-image: url({{asset($product->images[0]->path)}})"></span>
                                                            <br>
                                                            <span class="info-box-text text-center text-muted">{{\App\Hellper\GetLocalizedValue::GetValue($product, 'title')}}</span>
                                                            <br>
                                                            <span class="info-box-number text-center text-muted mb-0">Quantity: <input type="number" name="quantity[{{$product->id}}]" value=""></span>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary story-submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')

@endsection


