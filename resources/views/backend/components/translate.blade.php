<ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
    @foreach($countriesGLOBAL as $key => $item)
            {{--activ class in a teg--}}
            <li class="nav-item">
                <a class="nav-link  @if($key == 0) active @endif" id="custom-tabs-two-{{ $item->lang }}-tab" data-toggle="pill" href="#{{ $item->lang}}" role="tab" aria-controls="custom-tabs-two-{{ $item->lang }}" aria-selected="true">
                    {{ \App\Hellper\GetLocalizedValue::GetValue($item, 'name') }}
                </a>
            </li>
    @endforeach
</ul>
