<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/admin" class="brand-link">
        <img src="{{asset('assets/custom/img/ART_V6.png')}}" alt="AdminLTE Logo" class="elevation-3"
             style="opacity: .8;    margin: 0 auto;display: block; width: 100%;">
{{--        <span class="brand-text font-weight-light">Admin panel</span>--}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
{{--        <div class="user-panel mt-3 pb-3 mb-3 d-flex">--}}
{{--            <div class="image">--}}
{{--                @if(auth()->user()->type === 'admin')--}}
{{--                    <img src="{{asset('assets/custom/img/admin-logo1.png')}}" class="elevation-2" alt="User Image" style="width: 3.1rem;">--}}
{{--                @else--}}
{{--                    <img src="{{asset('assets/custom/img/support-logo1.png')}}" class="elevation-2" alt="User Image" style="width: 3.1rem;">--}}
{{--                @endif--}}
{{--            </div>--}}
{{--            <div class="info">--}}
{{--                <a href="#" class="d-block" style=" padding: 5px;">{{auth()->user()->name}}</a>--}}
{{--            </div>--}}
{{--        </div>--}}
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

                    <li class="nav-item">
                        <a href="{{route('country.index')}}" class="nav-link {{ Route::currentRouteNamed(['country.index', 'country.edit', 'country.create']) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-language" aria-hidden="true"></i>
                            <p>
                                Country
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('slider.index')}}" class="nav-link {{ Route::currentRouteNamed(['slider.index', 'slider.edit', 'slider.create']) ? 'active' : '' }}">
                            <i class="nav-icon far fa-images" aria-hidden="true"></i>
                            <p>
                                Slider
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('category.index')}}" class="nav-link {{ Route::currentRouteNamed(['category.index', 'category.edit', 'category.create']) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-balance-scale-left" aria-hidden="true"></i>
                            <p>
                                Category
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('product.index')}}" class="nav-link {{ Route::currentRouteNamed(['product.index', 'product.edit', 'product.create']) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-clipboard-list"></i>
                            <p>
                                Product
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('order.index')}}" class="nav-link {{ Route::currentRouteNamed(['order.index', 'order.edit', 'order.create']) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Orders
                                <span class="right badge badge-danger">{{$ordersGLOBAL}}</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('order.finished')}}" class="nav-link {{ Route::currentRouteNamed(['order.finished']) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-file-archive"></i>
                            <p>
                                Orders finished
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('application-proposal.index')}}" class="nav-link {{ Route::currentRouteNamed(['application-proposal.index']) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-newspaper"></i>
                            <p>
                                Application Proposal
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('user.index')}}" class="nav-link {{ Route::currentRouteNamed(['user.index', 'user.create', 'user.edit']) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                User
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('subscribe.index')}}" class="nav-link {{ Route::currentRouteNamed(['subscribe.index']) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-envelope"></i>
                            <p>
                                Subscribe
                            </p>
                        </a>
                    </li>



                    {{--                    <li class="nav-item">--}}
{{--                        <a href="{{url('/code/run/5')}}" class="nav-link">--}}
{{--                            <i class="nav-icon fas fa-user-tag"></i>--}}
{{--                            <p>--}}
{{--                                Cash clear--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                    </li>--}}


    {{-- ------------------------------------------------------------------------------------------------- --}}
{{--                    <li class="nav-item has-treeview menu-open">--}}
{{--                        <a href="#" class="nav-link active">--}}
{{--                            <i class="nav-icon fas fa-tachometer-alt"></i>--}}
{{--                            <p>--}}
{{--                                Dashboard--}}
{{--                                <i class="right fas fa-angle-left"></i>--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                        <ul class="nav nav-treeview">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="./index.html" class="nav-link active">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Dashboard v1</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="./index2.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Dashboard v2</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="./index3.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Dashboard v3</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="pages/widgets.html" class="nav-link">--}}
{{--                            <i class="nav-icon fas fa-th"></i>--}}
{{--                            <p>--}}
{{--                                Widgets--}}
{{--                                <span class="right badge badge-danger">New</span>--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item has-treeview">--}}
{{--                        <a href="#" class="nav-link">--}}
{{--                            <i class="nav-icon fas fa-copy"></i>--}}
{{--                            <p>--}}
{{--                                Layout Options--}}
{{--                                <i class="fas fa-angle-left right"></i>--}}
{{--                                <span class="badge badge-info right">6</span>--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                        <ul class="nav nav-treeview">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="pages/layout/top-nav.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Top Navigation</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="pages/layout/top-nav-sidebar.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Top Navigation + Sidebar</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
